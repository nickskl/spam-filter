#ifndef TESTCLASSIFIERINTERPRETER_H
#define TESTCLASSIFIERINTERPRETER_H

#include "classifiertaskinterpreter.h"
#include <iostream>

class TestClassifierTaskInterpreter : public ClassifierTaskInterpreter
{
public:
    TestClassifierTaskInterpreter(Classifier* _classifier)
        : ClassifierTaskInterpreter(_classifier) { }
    ~TestClassifierTaskInterpreter() { }

    bool AnyErrors() { return totalCount != (correctSpam + correctHam); }

    void ShowStatistics()
    {
        std::cout << "\nTotal:" << (correctSpam + correctHam) / (double)totalCount << "\nCorrect spam:" << correctSpam / (double)totalSpam <<  "\nCorrect ham: "
                  << correctHam / (double)totalHam  << "\n";
        Log("Total:" + std::to_string(totalCount) + "\nTotal spam:" + std::to_string(totalSpam) + "\nTotal:" + std::to_string(totalHam) +
            "\nCorrect spam:" + std::to_string(correctSpam) + "\nCorrect ham: " + std::to_string(correctHam) +
            "\nIncorrect spam:" + std::to_string(falseSpam) + "\nIncorrect ham:" + std::to_string(falseHam));
    }

    void ResetStatistics()
    {
        totalCount = 0;
        totalHam = 0;
        totalSpam = 0;
        correctHam = 0;
        correctSpam = 0;
        falseHam = 0;
        falseSpam = 0;
    }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator)
    {
        ClassificationResult expectedResult;
        auto correctClass = (*regexIterator)[1].str();
        auto path = (*regexIterator)[2].str();
        std::ifstream file;
        file.open(path);
        if(!file)
        {
            throw std::exception();
        }

        std::string text;
        file.seekg(0, std::ios::end);
        text.reserve(file.tellg());
        file.seekg(0, std::ios::beg);

        text.assign((std::istreambuf_iterator<char>(file)),
                    std::istreambuf_iterator<char>());
        file.close();

        if(correctClass == "spam")
            expectedResult = ClassificationResult::spam;
        else if (correctClass == "ham")
            expectedResult = ClassificationResult::nonSpam;
        else
        {
            throw std::exception();
        }
        auto result = classifier->Classify(text);

        totalCount++;
        if(expectedResult == ClassificationResult::spam)
        {
            totalSpam++;
            if(result == expectedResult)
                correctSpam++;
            else
                falseSpam++;
        }
        else
        {
            totalHam++;
            if(result == expectedResult)
                correctHam++;
            else
                falseHam++;
        }
    }

    unsigned long long GetTotalCount() const { return totalCount; }
    unsigned long long GetTotalSpam() const { return totalSpam; }
    unsigned long long GetTotalHam() const { return totalHam; }
    unsigned long long GetCorrectSpam() const { return correctSpam; }
    unsigned long long GetCorrectHam() const { return correctHam; }
    unsigned long long GetFalseSpam() const { return falseSpam; }
    unsigned long long GetFalseHam() const { return falseHam; }

protected:
    virtual void OnFinish()
    {
        ShowStatistics();
        Log("Finished test task: ");
        delete classifier;
    }

    virtual void OnStart() { ResetStatistics(); Log("Interpreting test task: "); }

    virtual std::regex RegEx() const
    {
        return std::regex("(\\w+) (.*)");
    }

private:
    unsigned long long totalCount = 0;
    unsigned long long correctSpam = 0;
    unsigned long long correctHam = 0;
    unsigned long long totalSpam = 0;
    unsigned long long totalHam = 0;
    unsigned long long falseSpam = 0;
    unsigned long long falseHam = 0;

};


#endif // TESTCLASSIFIERINTERPRETER_H
