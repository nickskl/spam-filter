#ifndef TUNECLASSIFIERTASKINTERPRETER_H
#define TUNECLASSIFIERTASKINTERPRETER_H
#include "classifiertaskinterpreter.h"
#include "testclassifierinterpreter.h"
#include "trainclassifiertaskinterpreter.h"

class TuneClassifierTaskInterpreter : public ClassifierTaskInterpreter
{
public:
    TuneClassifierTaskInterpreter(Classifier* _classifier, int _numberOfRetires, int _numberOfLearnCycles, double _learningDatasetSize)
        : ClassifierTaskInterpreter(_classifier), numberOfRetires(_numberOfRetires), numberOfLearnCycles(_numberOfLearnCycles), learningDatasetSize(_learningDatasetSize)
    { }

    ~TuneClassifierTaskInterpreter() {}
    virtual void Interpret(Task& task)
    {
        OnStart();
        TrainClassifierTaskInterpreter train(classifier);
        TestClassifierTaskInterpreter test(classifier);

        std::regex regex = RegEx();
        int numberOfTasks = task.GetTaskStrings().size() * learningDatasetSize;
        int counter = numberOfTasks >> 8;
        std::vector<std::string> taskStrings(numberOfTasks);
        int countLearnCycles = 1;
        do
        {
            std::cout << "Cycle: " << countLearnCycles << std::endl;
            task.ShuffleTasks();
            std::copy(task.GetTaskStrings().begin(), task.GetTaskStrings().begin() + numberOfTasks, taskStrings.begin());
            int countRetries = 1;
            do
            {
                std::cout << "Train:" << std::endl;
                std::random_shuffle(taskStrings.begin(), taskStrings.end());
                int progress = 0;
                for(auto string = taskStrings.begin(); string != taskStrings.end(); string++)
                {
                    std::regex_iterator<std::string::iterator> rxit ( string->begin(), string->end(), regex);
                    std::regex_iterator<std::string::iterator> rxend;

                    progress++;
                    if((counter == 0)||(progress % counter == 0))
                        printf("\r%3.5lf%%", progress*100.0/numberOfTasks);

                    while (rxit != rxend)
                    {
                       train.SpecificInterpretation(rxit);
                       rxit++;
                    }
                }
                printf("\r%3.5lf%%", progress*100.0/numberOfTasks);

                std::cout << std::endl << "Test:" << std::endl;
                test.ResetStatistics();
                progress = 0;
                for(auto string = taskStrings.begin(); string != taskStrings.end(); string++)
                {
                    std::regex_iterator<std::string::iterator> rxit ( string->begin(), string->end(), regex);
                    std::regex_iterator<std::string::iterator> rxend;

                    progress++;
                    if((counter == 0)||(progress % counter == 0))
                        printf("\r%3.5lf%%", progress*100.0/numberOfTasks);

                    while (rxit != rxend)
                    {
                       test.SpecificInterpretation(rxit);
                       rxit++;
                    }
                }
                printf("\r%3.5lf%%", progress*100.0/numberOfTasks);
                test.ShowStatistics();
                std::cout << std::endl;
            }
            while(test.AnyErrors()&&(countRetries++ < numberOfRetires));
        }
        while(countLearnCycles++ < numberOfLearnCycles);
        OnFinish();
    }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator) { regexIterator->size(); }

protected:
    virtual void OnFinish() { Log("Finished tune task: "); delete classifier; }
    virtual void OnStart() { Log("Interpreting tune task: "); }

    int numberOfRetires;
    int numberOfLearnCycles;
    double learningDatasetSize;

    virtual std::regex RegEx() const
    {
        return std::regex("(\\w+) (.*)");
    }
};

#endif // TUNECLASSIFIERTASKINTERPRETER_H
