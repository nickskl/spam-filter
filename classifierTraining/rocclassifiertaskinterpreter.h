#ifndef ROCCLASSIFIERTASKINTERPRETER_H
#define ROCCLASSIFIERTASKINTERPRETER_H

#include "classifiertaskinterpreter.h"
#include "testclassifierinterpreter.h"
#include <istream>

class ROCClassifierTaskInterpreter : public ClassifierTaskInterpreter
{
public:
    ROCClassifierTaskInterpreter(Classifier* _classifier, double _step)
        : ClassifierTaskInterpreter(_classifier), test(_classifier), step(_step) { }

    ~ROCClassifierTaskInterpreter() {}

    virtual void Interpret(Task& task)
    {
        OnStart();
        std::regex regex = RegEx();
        auto taskStrings = task.GetTaskStrings();
        auto numberOfTasks = task.GetTaskStrings().size();

        file.open(classifier->GetDictionaryName() + ".roc");
        unsigned long long truePositive = 0;
        unsigned long long falsePositive = 0;
        file << "threshold\tTPR\tFPR" << std::endl;

        for(double threshold = 0.0; threshold < 1.0001; threshold += step)
        {
            if(threshold > 1.0)
                threshold = 1.0;
            std::cout << std::endl <<  "Threshold: " << threshold << std::endl;
            classifier->Threshold() = threshold;
            double progress = 0;
            for(auto string = taskStrings.begin(); string != taskStrings.end(); string++)
            {
                std::regex_iterator<std::string::iterator> rxit ( string->begin(), string->end(), regex);
                std::regex_iterator<std::string::iterator> rxend;

                printf("\r%3.5lf%%", ++progress/numberOfTasks*100);

                while (rxit != rxend)
                {
                   test.SpecificInterpretation(rxit);
                   rxit++;
                }
            }
            truePositive = test.GetCorrectSpam();
            falsePositive = test.GetFalseSpam();
            file << threshold << "\t" << (double)truePositive / test.GetTotalSpam() << "\t" <<
                    (double)falsePositive / test.GetTotalSpam() << std::endl;
            test.ResetStatistics();
        }
        OnFinish();
    }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator) { *regexIterator; }

protected:
    virtual void OnFinish() { Log("Finished roc task"); file.close(); delete classifier; }

    virtual void OnStart() { Log("Interpreting roc task"); }

    virtual std::regex RegEx() const
    {
        return std::regex("(\\w+) (.*)");
    }

    std::ofstream file;
    TestClassifierTaskInterpreter test;
    double step;
};


#endif // ROCCLASSIFIERTASKINTERPRETER_H
