#include "configuration.h"

int main(int argc, char *argv[])
{
    std::srand(std::time(0));
    std::string configurationFile;
    std::string task;
    if(argc > 1)
        task = argv[1];
    else
        throw std::exception();

    if(argc == 3)
        configurationFile = argv[2];
    else
        configurationFile = Configuration::defaultConfigurationFilename;

    Configuration config(configurationFile);
    TaskManager manager(config.CreateTasks(task));

    manager.RunTasks();

    return 0;
}
