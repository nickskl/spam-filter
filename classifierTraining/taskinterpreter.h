#ifndef TASKINTERPRETER_H
#define TASKINTERPRETER_H

#include "task.h"

class TaskInterpreter
{
public:
    virtual ~TaskInterpreter() {}
    virtual void Interpret(Task& task) = 0;
protected:
    virtual void Log(const std::string& string, const std::string& filename)
    {
        std::ofstream file;
        file.open(filename, std::ios_base::out | std::ios_base::app);
        time_t t = time(0);
        struct tm * now = localtime(&t);
        file << "[" << now->tm_mday << "." << now->tm_mon << "." << now->tm_year << " " <<
                now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << "]\n";
        file << string << std::endl << std::endl;
        file.close();
    }
};

#endif // TASKINTERPRETER_H
