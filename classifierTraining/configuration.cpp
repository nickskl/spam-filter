#include "configuration.h"

const std::string Configuration::defaultConfigurationFilename = "config";

Configuration::Configuration(const std::string& path)
{
    std::ifstream file(path);

    if(!file)
    {
        file.close();
        CreateNewConfiguration(path);
        file.open(path);
    }
    file.seekg(0, std::ios::end);
    configuraion.reserve(file.tellg());
    file.seekg(0, std::ios::beg);

    configuraion.assign((std::istreambuf_iterator<char>(file)),
                std::istreambuf_iterator<char>());
    file.close();
}

void Configuration::Load(const std::string& path)
{
    std::ifstream file(path);

    file.seekg(0, std::ios::end);
    configuraion.reserve(file.tellg());
    file.seekg(0, std::ios::beg);

    configuraion.assign((std::istreambuf_iterator<char>(file)),
                std::istreambuf_iterator<char>());
    file.close();
}

void Configuration::Save(const std::string& path)
{
    std::ofstream file(path);

    file << configuraion;

    file.close();
}

std::vector<TaskManagerEntry*> Configuration::CreateTasks(const std::string &taskName)
{
    auto task = GetSettings(taskName, GetSettings("tasks"));
    auto taskType = GetType(task);
    auto taskParameters = GetParameters(task);
    SupportedModes mode = SupportedModes::stub;
    std::vector<std::string> t;
    std::vector<TaskManagerEntry *> result;

    if(taskType == "train")
    {
        mode = SupportedModes::train;
    }
    else if(taskType == "test")
    {
        mode = SupportedModes::test;
    }
    else if(taskType == "tune")
    {
        mode = SupportedModes::tune;
    }
    else if(taskType == "classify")
    {
        mode = SupportedModes::classify;
    }
    else if(taskType == "roc")
    {
        mode = SupportedModes::roc;
    }
    else if(taskType == "task-chain")
    {
        mode = SupportedModes::taskChain;
        if(taskParameters.find("chain") != taskParameters.end())
        {
            auto tasks = taskParameters["chain"];
            std::regex regex("\\t*([\\w.-]+)\\n?");
            std::regex_iterator<std::string::const_iterator> rxit (tasks.begin(), tasks.end(), regex);
            std::regex_iterator<std::string::const_iterator> rxend;
            if(rxit != rxend)
            {
                while(rxit != rxend)
                {
                    auto taskName = (*rxit)[1].str();
                    auto newTasks = CreateTasks(taskName);
                    result.insert(result.end(), newTasks.begin(), newTasks.end());
                    rxit++;
                }
                return result;
            }
            else
                throw std::exception();
        }
        else
            throw std::exception();
    }
    else
    {
        throw std::exception();
    }

    FeatureExtractor* fe = nullptr;
    if(taskParameters.find("feature-extractor") != taskParameters.end())
    {
        fe = CreateFeatureExtractor(taskParameters["feature-extractor"]);
    }
    else
        throw std::exception();

    Classifier* classifier = CreateClassifier(fe);

    int numberOfRetries = 2;
    if(taskParameters.find("number-of-retries") != taskParameters.end())
    {
        numberOfRetries = std::stoi(taskParameters["number-of-retries"]);
    }

    int numberOfLearnCycles = 1;
    if(taskParameters.find("number-of-learn-cycles") != taskParameters.end())
    {
        numberOfLearnCycles = std::stoi(taskParameters["number-of-learn-cycles"]);
    }

    double learningDatasetSize = 0.25;
    if(taskParameters.find("learning-dataset-size") != taskParameters.end())
    {
        learningDatasetSize = std::stod(taskParameters["learning-dataset-size"]);
    }

    double step = 0.1;
    if(taskParameters.find("step") != taskParameters.end())
    {
        step = std::stof(taskParameters["step"]);
    }

    if(taskParameters.find("taskfile-path") != taskParameters.end())
    {
        std::ifstream file(taskParameters["taskfile-path"]);
        if(!file)
            throw std::exception();
        char buffer[256];
        while(file)
        {
            file.getline(buffer, 256);
            if(buffer[0] == '\0')
                continue;
            buffer[file.gcount()] = 0;
            t.push_back(buffer);
        }
        file.close();
    }
    else if(taskParameters.find("inline-taskfile") != taskParameters.end())
    {
        auto task = taskParameters["inline-taskfile"];
        std::regex regex("\\t*(.+)\\n?");
        std::regex_iterator<std::string::const_iterator> rxit (task.begin(), task.end(), regex);
        std::regex_iterator<std::string::const_iterator> rxend;
        if(rxit != rxend)
        {
            while(rxit != rxend)
            {
                t.push_back((*rxit)[1].str());
                rxit++;
            }
        }
        else
            throw std::exception();
    }
    else
        throw std::exception();
    switch (mode)

    {
    case SupportedModes::test:
        result.push_back(new TaskManagerEntry(Task(t), new TestClassifierTaskInterpreter(classifier)));
        break;
    case SupportedModes::train:
        result.push_back(new TaskManagerEntry(Task(t), new TrainClassifierTaskInterpreter(classifier)));
        break;
    case SupportedModes::tune:
        result.push_back(new TaskManagerEntry(Task(t), new TuneClassifierTaskInterpreter(classifier, numberOfRetries, numberOfLearnCycles, learningDatasetSize)));
        break;
    case SupportedModes::classify:
        result.push_back(new TaskManagerEntry(Task(t), new ClassifyClassifierTaskInterpreter(classifier)));
        break;
    case SupportedModes::roc:
        result.push_back(new TaskManagerEntry(Task(t), new ROCClassifierTaskInterpreter(classifier, step)));
        break;
    default:
        throw std::exception();
        break;
    }

    return result;
}

FeatureExtractor* Configuration::CreateFeatureExtractor(const std::string& feSettings)
{
    auto feType = GetType(feSettings);
    auto parameters = GetParameters(feSettings);

    if(feType == "mfe")
    {
        return new MarkovianFeatureExtractor(std::stoi(parameters["k"]));
    }
    else if(feType == "osb")
    {
        return new OSBFeatureExtractor(std::stoi(parameters["k"]));
    }
    else if(feType == "cng")
    {
        return new CharacterNGrammFeatureExtractor(std::stoi(parameters["k"]));
    }
    else
    {
        throw std::exception();
    }

    return nullptr;
}

Classifier* Configuration::CreateClassifier(FeatureExtractor* featureExtractor)
{
    auto classifierSettings = GetSettings("classifier");
    auto classifierType = GetType(classifierSettings);
    if(classifierType == "nsnb")
    {
        auto settings = GetParameters(classifierSettings);
        return new NSNBClassifier("NSNB-" + DictionaryNaming(featureExtractor), featureExtractor, std::stod(settings["threshold"]), std::stod(settings["epsilon"]),
                std::stod(settings["delta"]), std::stod(settings["alpha"]), std::stod(settings["beta"]), std::stoul(settings["number-of-features-to-consider"]));
    }
    else if (classifierType == "nb")
    {
        auto settings = GetParameters(classifierSettings);
        return new NBClassifier("NB-" + DictionaryNaming(featureExtractor), featureExtractor, std::stod(settings["threshold"]), std::stod(settings["epsilon"]),
                std::stoul(settings["number-of-features-to-consider"]));
    }
    else if (classifierType == "modnsnb")
    {
        auto settings = GetParameters(classifierSettings);
        return new modNSNBClassifier("modNSNB-" + DictionaryNaming(featureExtractor), featureExtractor, std::stod(settings["threshold"]), std::stod(settings["epsilon"]),
                std::stod(settings["delta"]), std::stod(settings["alpha"]), std::stod(settings["beta"]), std::stoul(settings["number-of-features-to-consider"]));
    }
    else
    {
        throw std::exception();
    }
}

void Configuration::CreateNewConfiguration(const std::string& path)
{
    configuraion = "classifier:\n\ttype: nsnb\n\tparameters:\n\t\tthreshold: 0.5\n\t\tepsilon: 1E-5\n\t\t\
delta: 0.25\n\t\talpha: 0.5\n\t\tbeta: 0.5\n\t\tnumber-of-features-to-consider: -1\n\
tasks:\n\ttrain:\n\t\ttype: train\n\t\tparameters:\n\t\t\ttaskfile-path: ./train\n\
\t\t\tfeature-extractor:\n\t\t\t\ttype: osb\n\t\t\t\tparameters:\n\t\t\t\t\tk: 5\n";
    Save(path);
}

std::string Configuration::GetSettings(const std::string& field) const
{
    std::regex regex("(\\t*)" + field + ":\n((?:\\1\\t+(?:[\\S ]+)(?:\\n|$))+)");
    std::regex_iterator<std::string::const_iterator> rxit( configuraion.begin(), configuraion.end(), regex);
    std::regex_iterator<std::string::const_iterator> rxend;

    if(rxit != rxend)
        return (*rxit)[2].str();
    else
        throw std::exception();
}

std::string Configuration::GetSettings(const std::string& field, const std::string& string) const
{
    std::regex regex("(\\t*)" + field + ":\n((?:\\1\\t+(?:[\\S ]+)(?:\\n|$))+)");
    std::regex_iterator<std::string::const_iterator> rxit( string.begin(), string.end(), regex);
    std::regex_iterator<std::string::const_iterator> rxend;

    if(rxit != rxend)
        return (*rxit)[2].str();
    else
        throw std::exception();
}

std::string Configuration::GetType(const std::string& string) const
{
    std::regex regex("type:[ \\t]*([\\w.-]+)");
    std::regex_iterator<std::string::const_iterator> rxit( string.begin(), string.end(), regex);
    std::regex_iterator<std::string::const_iterator> rxend;

    if(rxit != rxend)
        return (*rxit)[1].str();
    else
        throw std::exception();
}

std::string Configuration::__GetParameters(const std::string& string) const
{
    std::regex regex("(\\t*)parameters:((?:\\n\\1\\t.+:(?:(?:\\n\\1\\t\\t)?.+)+)+)");
    std::regex_iterator<std::string::const_iterator> rxit ( string.begin(), string.end(), regex);
    std::regex_iterator<std::string::const_iterator> rxend;

    if(rxit != rxend)
        return (*rxit)[2].str();
    else
        throw std::exception();
}

std::map<std::string, std::string> Configuration::GetParameters(const std::string& string)
{
    auto parameters = __GetParameters(string);
    std::regex regex("(\\t*)(.+):[ \\n]*((?:.+(?:\\n\\1\\t)?)+)");
    std::regex_iterator<std::string::const_iterator> rxit (parameters.begin(), parameters.end(), regex);
    std::regex_iterator<std::string::const_iterator> rxend;
    if(rxit != rxend)
    {
        std::map<std::string, std::string> result;
        while(rxit != rxend)
        {
            result.insert(std::make_pair<std::string, std::string>((*rxit)[2].str(), (*rxit)[3].str()));
            rxit++;
        }
        return result;
    }
    else
        throw std::exception();
}
