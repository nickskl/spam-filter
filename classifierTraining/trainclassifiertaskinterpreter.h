#ifndef TRAINCLASSIFIERTASKINTERPRETER_H
#define TRAINCLASSIFIERTASKINTERPRETER_H

#include "classifiertaskinterpreter.h"
#include <iostream>

class TrainClassifierTaskInterpreter : public ClassifierTaskInterpreter
{
public:
    TrainClassifierTaskInterpreter(Classifier* _classifier)
        : ClassifierTaskInterpreter(_classifier) { }
    ~TrainClassifierTaskInterpreter() { }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator)
    {
        ClassificationResult expectedResult;
        auto correctClass = (*regexIterator)[1].str();
        auto path = (*regexIterator)[2].str();
        std::ifstream file;
        file.open(path);
        if(!file)
        {
            throw std::exception();
        }

        std::string text;
        file.seekg(0, std::ios::end);
        text.reserve(file.tellg());
        file.seekg(0, std::ios::beg);

        text.assign((std::istreambuf_iterator<char>(file)),
                    std::istreambuf_iterator<char>());
        file.close();

        if(correctClass == "spam")
            expectedResult = ClassificationResult::spam;
        else if (correctClass == "ham")
            expectedResult = ClassificationResult::nonSpam;
        else
        {
            throw std::exception();
        }
        classifier->Classify(text, expectedResult);
    }

protected:
    virtual void OnFinish() { Log("Finished train task: "); delete classifier; }
    virtual void OnStart() { Log("Interpreting train task"); }
    virtual std::regex RegEx() const
    {
        return std::regex("(\\w+) (.*)");
    }
};

#endif // TRAINCLASSIFIERTASKINTERPRETER_H
