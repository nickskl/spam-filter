#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <string>
#include <vector>
#include <exception>
#include <regex>
#include "featureextractor.h"
#include "classifier.h"
#include "taskmanager.h"
#include "trainclassifiertaskinterpreter.h"
#include "testclassifierinterpreter.h"
#include "nsnbclassifier.h"
#include "tuneclassifiertaskinterpreter.h"
#include "classifyclassifiertaskinterpreter.h"
#include "trainclassifiertaskinterpreter.h"
#include "nsnbclassifier.h"
#include "markovianfeatureextractor.h"
#include "osbfeatureextractor.h"
#include "characterngrammfeatureextractor.h"
#include "rocclassifiertaskinterpreter.h"
#include "nbclassifier.h"

enum class SupportedModes { stub, train, test, tune, classify, roc, taskChain };

class Configuration
{
public:
    const static std::string defaultConfigurationFilename;

    Configuration(const std::string& path);

    void Load(const std::string& path);
    void Save(const std::string& path);

    std::vector<TaskManagerEntry *> CreateTasks(const std::string& taskName);

private:
    void CreateNewConfiguration(const std::string& path);
    FeatureExtractor* CreateFeatureExtractor(const std::string& feSettings);
    Classifier* CreateClassifier(FeatureExtractor* featureExtractor);

    std::string GetSettings(const std::string& field) const;
    std::string GetSettings(const std::string& field, const std::string& string) const;
    std::string GetType(const std::string& string) const;
    std::string __GetParameters(const std::string& string) const;
    std::map<std::string, std::string> GetParameters(const std::string& string);

    std::string configuraion;
    std::string defaultDictionaryNaming = ".dct";
    std::string DictionaryNaming(FeatureExtractor* fe) { return fe->Signature() + defaultDictionaryNaming; }
};

#endif // CONFIGURATION_H
