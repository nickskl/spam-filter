#ifndef CLASSIFYCLASSIFIERTASKINTERPRETER_H
#define CLASSIFYCLASSIFIERTASKINTERPRETER_H
#include "classifiertaskinterpreter.h"
#include <istream>

class ClassifyClassifierTaskInterpreter : public ClassifierTaskInterpreter
{
public:
    ClassifyClassifierTaskInterpreter(Classifier* _classifier)
        : ClassifierTaskInterpreter(_classifier) { }

    ~ClassifyClassifierTaskInterpreter() {}

    virtual void Interpret(Task& task)
    {
        Log("Interpreting classification task: ");
        std::regex regex = RegEx();
        for(auto string = task.GetTaskStrings().begin(); string != task.GetTaskStrings().end(); string++)
        {
            std::regex_iterator<std::string::const_iterator> rxit(string->begin(), string->end(), regex);
            std::regex_iterator<std::string::const_iterator> rxend;
            if(rxit != rxend)
            {
                auto path = (*rxit)[1].str();
                std::ifstream file;
                file.open(path);
                if(!file)
                {
                    throw std::exception();
                }

                std::string text;
                file.seekg(0, std::ios::end);
                text.reserve(file.tellg());
                file.seekg(0, std::ios::beg);

                text.assign((std::istreambuf_iterator<char>(file)),
                            std::istreambuf_iterator<char>());
                file.close();

                auto result = classifier->Classify(text);

                switch(result)
                {
                case ClassificationResult::spam:
                    std::cout << path <<  " - spam" << std::endl;
                    Log(path + " - spam");
                    break;
                case ClassificationResult::nonSpam:
                    std::cout << path <<  " - ham" << std::endl;
                    Log(path + " - ham");
                    break;
                case ClassificationResult::none:
                    std::cout << "Oops! " << path <<  " - none" << std::endl;
                    Log("Oops! " + path + " - none");
                    break;
                }
            }
        }
        OnFinish();
    }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator) { regexIterator->size(); }

protected:
    virtual void OnFinish() { delete classifier; }
    virtual void OnStart() {}

    virtual std::regex RegEx() const
    {
        return std::regex("(.*)");
    }
};

#endif // CLASSIFYCLASSIFIERTASKINTERPRETER_H
