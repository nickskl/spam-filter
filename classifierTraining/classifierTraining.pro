QT += core
QT -= gui

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11 -ggdb

TARGET = classifierTraining
CONFIG += console
CONFIG -= app_bundle

LIBS += -L../classifier/release -lclassifier

TEMPLATE = app

INCLUDEPATH += ../classifier
DEPENDPATH += ../classifier

SOURCES += main.cpp \
    configuration.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    ../classifier/characterngrammfeatureextractor.h \
    ../classifier/classifier.h \
    ../classifier/classifier_global.h \
    ../classifier/dictionary.h \
    ../classifier/feature.h \
    ../classifier/featureextractor.h \
    ../classifier/hash.h \
    ../classifier/hash.hpp \
    ../classifier/markovianfeatureextractor.h \
    ../classifier/nsnbclassifier.h \
    ../classifier/osbfeatureextractor.h \
    task.h \
    taskinterpreter.h \
    taskmanager.h \
    trainclassifiertaskinterpreter.h \
    classifiertaskinterpreter.h \
    configuration.h \
    testclassifierinterpreter.h \
    tuneclassifiertaskinterpreter.h \
    classifyclassifiertaskinterpreter.h \
    rocclassifiertaskinterpreter.h
