#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include "task.h"
#include "taskinterpreter.h"
#include <set>

class TaskManagerEntry
{
public:
    TaskManagerEntry(const Task& _task, TaskInterpreter* _interpreter)
        : task(_task), interpreter(_interpreter) { }
    ~TaskManagerEntry() { delete interpreter; }

    void Interpret()
    {
        if(interpreter == nullptr) throw std::exception();
        interpreter->Interpret(task);
    }

private:
    Task task;
    TaskInterpreter* interpreter;
};

class TaskManager
{
public:
    TaskManager() { }
    TaskManager(const std::vector<TaskManagerEntry *>& _tasks)
    {
        for(auto t = _tasks.begin(); t != _tasks.end(); t++)
            tasks.push(*t);
    }

    ~TaskManager()
    {
        while(!tasks.empty())
        {
            delete tasks.front();
            tasks.pop();
        }
    }

    void AddNewTask(const Task& task, TaskInterpreter* ti)
    {
        tasks.push(new TaskManagerEntry(task, ti));
    }

    void AddNewTask(TaskManagerEntry* entry)
    {
        tasks.push(entry);
    }

    void RunTasks()
    {
        while(!tasks.empty())
        {
            TaskManagerEntry* task = tasks.front();
            task->Interpret();
            delete task;
            tasks.pop();
        }
    }

private:
    std::queue<TaskManagerEntry*> tasks;
};

#endif // TASKMANAGER_H
