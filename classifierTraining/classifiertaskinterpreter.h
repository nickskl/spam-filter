#ifndef TESTCLASSIFIERTASKINTERPRETER_H
#define TESTCLASSIFIERTASKINTERPRETER_H

#include "taskinterpreter.h"
#include <regex>
#include <fstream>

class ClassifierTaskInterpreter : public TaskInterpreter
{
public:
    ClassifierTaskInterpreter(Classifier* _classifier)
        : classifier(_classifier) { }

    ~ClassifierTaskInterpreter() { }

    virtual void Interpret(Task& task)
    {
        std::regex regex = RegEx();
        auto taskStrings = task.GetTaskStrings();
        auto numberOfTasks = task.GetTaskStrings().size();
        double progress = 0;
        OnStart();
        for(auto string = taskStrings.begin(); string != taskStrings.end(); string++)
        {
            std::regex_iterator<std::string::iterator> rxit ( string->begin(), string->end(), regex);
            std::regex_iterator<std::string::iterator> rxend;

            printf("\r%3.5lf%%", ++progress/numberOfTasks*100);

            while (rxit != rxend)
            {
               SpecificInterpretation(rxit);
               rxit++;
            }
        }
        OnFinish();
    }

    virtual void SpecificInterpretation(const std::regex_iterator<std::string::iterator>& regexIterator) = 0;
protected:

    virtual void Log(const std::string& string)
    {
        TaskInterpreter::Log(string,  classifier->GetDictionaryName() + "-log.txt");
    }

    virtual void OnFinish() = 0;
    virtual void OnStart() = 0;
    virtual std::regex RegEx() const = 0;
    Classifier* classifier;
};

#endif // TESTCLASSIFIERTASKINTERPRETER_H
