#ifndef TASK_H
#define TASK_H

#include <string>
#include <queue>
#include "classifier.h"

class Task
{
public:
    Task(const std::vector<std::string>& _taskStrings) : taskStrings(_taskStrings) {}
    Task(const Task& task) { taskStrings = task.taskStrings; }

    Task(Task&& task) { taskStrings = task.taskStrings; }

    Task& operator=(const Task& task)
    {
        taskStrings = task.taskStrings;
        return *this;
    }

    void ShuffleTasks()
    {
        std::random_shuffle(taskStrings.begin(), taskStrings.end());
    }

    const std::vector<std::string>& GetTaskStrings() const { return taskStrings; }
private:
    std::vector<std::string> taskStrings;
};

#endif // TASK_H
