#include "feature.h"

Feature::Feature(std::string _data, unsigned long _weight)
    : data(_data), weight(_weight)
{
}

const std::string& Feature::GetData() const
{
    return this->data;
}

std::string& Feature::GetData()
{
    return this->data;
}

void Feature::SetData(const std::string &_data)
{
    this->data = _data;
}

unsigned long Feature::GetWeight() const
{
    return this->weight;
}

