#include "dictionary.h"

const double DictionaryEntry::min = 1E-8;
const double DictionaryEntry::max = 4096;
const unsigned long long Dictionary::defaultMaxNumberOfEntries = 12000000;

std::istream& operator>>(std::istream& in, DictionaryEntry& obj)
{
    size_t size;
    in.read(reinterpret_cast<char*>(&obj.spamCount), sizeof(obj.spamCount));
    in.read(reinterpret_cast<char*>(&obj.hamCount), sizeof(obj.hamCount));
    in.read(reinterpret_cast<char*>(&obj.confidenceFactor), sizeof(obj.confidenceFactor));
    in.read(reinterpret_cast<char*>(&size), sizeof(size));
    char buffer[size+1];
    in.read(buffer, size);
    buffer[size] = '\0';
    obj.key.assign(buffer, size);

    return in;
}

std::ostream& operator<<(std::ostream& out, const DictionaryEntry& obj)
{
    out.write(reinterpret_cast<const char*>(&obj.spamCount), sizeof(obj.spamCount));
    out.write(reinterpret_cast<const char*>(&obj.hamCount), sizeof(obj.hamCount));
    out.write(reinterpret_cast<const char*>(&obj.confidenceFactor), sizeof(obj.confidenceFactor));
    size_t size = obj.key.length();
    out.write(reinterpret_cast<const char*>(&size), sizeof(size));
    out.write(obj.key.c_str(), size);

    return out;
}

void DictionaryEntry::ChangeConfidence(double factor)
{
    confidenceFactor *= factor;
    if (confidenceFactor < min)
        confidenceFactor = min;
    else if (confidenceFactor > max)
        confidenceFactor = max;
}

Dictionary::Dictionary(const std::string &_signature, const unsigned long long _maxNumberOfEntries, const unsigned long long _maxHashTableSize)
    : signature(_signature + LIB_VERSION), hashTable(HashTable<std::string, DictionaryEntry>(_maxHashTableSize)), totalSpamCount(0), totalHamCount(0), maxNumberOfEntries(_maxNumberOfEntries)
{

}

DictionaryEntry* Dictionary::GetEntry(const std::string& key)
{
    try
    {
        std::vector<DictionaryEntry>& tmp = hashTable.Get(key);
        for(auto i = tmp.begin(); i != tmp.end(); i++)
        {
            if(i->Key() == key)
            {
                return &(*i);
            }
        }
        return nullptr;
    }
    catch(HashTableEntryNotFoundException& e)
    {
        return nullptr;
    }
}

const DictionaryEntry* Dictionary::GetEntry(const std::string& key) const
{
    try
    {
        const std::vector<DictionaryEntry>& tmp = hashTable.Get(key);
        for(auto i = tmp.begin(); i != tmp.end(); i++)
        {
            if(i->Key() == key)
            {
                return &(*i);
            }
        }
        return nullptr;
    }
    catch(HashTableEntryNotFoundException& e)
    {
        return nullptr;
    }
}

const DictionaryEntry* Dictionary::CreateEntry(const std::string& key)
{
    if(hashTable.NumberOfEntries() < maxNumberOfEntries)
    {
        return hashTable.Add(key, DictionaryEntry(key));
    }
    return nullptr;
}
const DictionaryEntry* Dictionary::AddEntry(const DictionaryEntry& entry)
{
    if(hashTable.NumberOfEntries() < maxNumberOfEntries)
    {
        return hashTable.Add(entry.Key(), entry);
    }
    return nullptr;
}

void Dictionary::DeleteEntry(const DictionaryEntry& entry)
{
    hashTable.Delete(entry.Key(), entry);
}

void Dictionary::DeleteWeakFeatures()
{
    for(auto i = hashTable.data.begin(); i != hashTable.data.end(); i++)
    {
        if((!(*i).empty()))
        {
            std::vector<DictionaryEntry*> toDelete;
            for(auto j = i->begin(); j != i->end(); j++)
            {
                if(((j->ConfidenceFactor() < 1.05)&&(j->ConfidenceFactor() > 0.95)))
                    toDelete.push_back(&(*j));
            }
            for(auto entry = toDelete.rbegin(); entry != toDelete.rend(); entry++)
            {
                DeleteEntry(**entry);
            }
        }
    }
}


void Dictionary::CreateDictionary(const std::string &path)
{
    SaveToFile(path);
}

void Dictionary::SaveToFile(const std::string& path)
{
    std::ofstream output;
    output.open(path, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);

    if(output)
    {
        size_t length = signature.length();
        output.write(reinterpret_cast<char*>(&length), sizeof(length));
        output.write(signature.c_str(), length);
        output.write(reinterpret_cast<char*>(&maxNumberOfEntries), sizeof(maxNumberOfEntries));
        output.write(reinterpret_cast<char*>(&totalSpamCount), sizeof(totalSpamCount));
        output.write(reinterpret_cast<char*>(&totalHamCount), sizeof(totalHamCount));
        hashTable.WriteToFile(output);
    }
    else
    {
        // throw DictionaryFileNotFoundException("Unable to open the file");
    }
}

void Dictionary::LoadFromFile(const std::__cxx11::string &path)
{
    std::ifstream input;
    input.open(path, std::ios_base::in | std::ios_base::binary);

    if(input)
    {
        size_t length;
        char buffer[64];
        input.read(reinterpret_cast<char*>(&length), sizeof(size_t));
        input.read(buffer, length);
        buffer[length] = '\0';
        if(signature != buffer)
        {
            throw DictionaryException("Wrong dictionary signature");
        }
        input.read(reinterpret_cast<char*>(&maxNumberOfEntries), sizeof(maxNumberOfEntries));
        input.read(reinterpret_cast<char*>(&totalSpamCount), sizeof(totalSpamCount));
        input.read(reinterpret_cast<char*>(&totalHamCount), sizeof(totalHamCount));
        hashTable.ReadFromFile(input);
    }
    else
    {
        CreateDictionary(path);
    }
}
