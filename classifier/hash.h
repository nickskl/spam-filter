#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <hashtable.h>
#include <exception>
#include <vector>
#include <fstream>
#include <iterator>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT HashTableException : public std::exception
{
public:
    HashTableException(const std::string& _buffer) : buffer(_buffer) {}
    const char* what() const noexcept { return buffer.c_str();}
protected:
    const std::string buffer;
};

class CLASSIFIERSHARED_EXPORT HashTableEntryNotFoundException : public HashTableException
{
public:
    HashTableEntryNotFoundException(const std::string& _buffer) : HashTableException(_buffer) {}
};

template <typename Key, typename Data>
class CLASSIFIERSHARED_EXPORT HashTable
{
friend class Dictionary;
public:
    HashTable(unsigned int _size = defaultSize);
    ~HashTable();
    const Data* Add(const Key &key, const Data &entry);
    void Delete(const Key &key, const Data &entry);
    const std::vector<Data> &Get(const Key& key) const;
    std::vector<Data> &Get(const Key& key);
    const Data &GetEntry(const Key& key, const Data& entry) const;
    Data &GetEntry(const Key& key, const Data& entry);
    const Data* Contains(const Key& key, const Data& entry) const;
    unsigned long long NumberOfEntries() const { return numberOfEntries; }

    void WriteToFile(std::ofstream& file) const;
    void ReadFromFile(std::ifstream &file);

private:
    unsigned long size;
    std::vector<std::vector<Data>> data;
    unsigned long HashFunction(const Key& key) const;
    static const unsigned long defaultSize = 12000017;
    unsigned long long numberOfEntries;
};

#include "hash.hpp"

#endif // HASHTABLE_H
