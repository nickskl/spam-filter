#include "nbclassifier.h"

const double NBClassifier::defaultThreshold = 0.5;
const double NBClassifier::defaultEpsilon = 1E-5;
const std::string NBClassifier::defaultDictionaryName = "dictionary.dct";
const unsigned long NBClassifier::defaultNumberOfFeaturesToConsider = -1;
const unsigned long NBClassifier::maxNumberOfIterations = 512;

ClassificationResult NBClassifier::Classify(const std::string& text, const ClassificationResult expectedResult)
{
    if (featureExtractor == nullptr)
    {
        throw ClassifierNoFeatureExtractorSelectedException();
    }
    auto features = featureExtractor->Extract(text);
    bool training = false;

    if(expectedResult != ClassificationResult::none)
    {
        training = true;
    }
    double criteria;

    ClassificationResult result;

    if (!needToSaveDictionary)
    {
        return Classify(features, criteria);
    }

    if(training)
    {
        result = Classify(features, criteria, defaultEpsilon, true);
        unsigned int numberOfIterations = 0;
        while((numberOfIterations < maxNumberOfIterations)&&(result != expectedResult))
        {
            Train(features, expectedResult);
            result = Classify(features, criteria);
            numberOfIterations++;
        }
        if((expectedResult == ClassificationResult::spam))
            dictionary.IncSpam();
        else
            dictionary.IncHam();
    }
    else
        result = Classify(features, criteria);

    return result;
}

void NBClassifier::SwitchDictionary(const std::string& _dictionaryName)
{
    SaveDictionary();
    LoadDictionary(_dictionaryName);
}

void NBClassifier::LoadDictionary(const std::string& _dictionaryName)
{
    dictionary.LoadFromFile(_dictionaryName);
    dictionaryName = _dictionaryName;
}

void NBClassifier::SaveDictionary()
{
    dictionary.SaveToFile(dictionaryName);
}

void NBClassifier::Train(const std::list<Feature> &features, const ClassificationResult expectedResut)
{
    if((expectedResut == ClassificationResult::spam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                f->IncSpam();
            }
        }
    }
    else if((expectedResut == ClassificationResult::nonSpam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                f->IncHam();
            }
        }
    }
}

ClassificationResult NBClassifier::Classify(const std::list<Feature>& features, double& criteria, const double epsilon, bool createNewEntry)
{
    double totalProb = 0;
    unsigned long count = 0;
    unsigned long maxWeight = 1;
    if(featureExtractor->isWeighted())
    {
        maxWeight = 4<<(featureExtractor->k);
    }
    std::multiset<double> probSet;

    for(auto feature = features.begin(); feature != features.end(); feature++)
    {
        const DictionaryEntry* entry = dictionary.GetEntry(feature->GetData());
        if(entry == nullptr)
        {
            if(!createNewEntry)
                continue;
            entry = dictionary.CreateEntry(feature->GetData());
            if(!entry)
                continue;
        }

        double prob = (entry->SpamCount() + epsilon)/(entry->HamCount() + epsilon)*feature->GetWeight()/(double)maxWeight;
        probSet.insert(std::log(prob));
    }

    for(auto prob = probSet.rbegin(); (count != numberOfFeaturesToConsider)&&(prob != probSet.rend()); count++, prob++)
    {
        totalProb += *prob;
    }

    totalProb += (features.size()-1)*log(((double)dictionary.GetTotalHamCount()+epsilon)/(dictionary.GetTotalSpamCount() + epsilon));

    criteria = 1.0 / (1.0 + std::exp(-totalProb));

    if(criteria > threshold)
        return ClassificationResult::spam;
    else
        return ClassificationResult::nonSpam;
}

void NBClassifier::DeleteWeakFeatures() { }
