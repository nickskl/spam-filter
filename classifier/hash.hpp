template <typename Key, typename Data>
HashTable<Key, Data>::HashTable(unsigned int _size)
    : size(_size), numberOfEntries(0)
{
    data.resize(size);
}

template <typename Key, typename Data>
HashTable<Key, Data>::~HashTable()
{
}

template <typename Key, typename Data>
const Data* HashTable<Key, Data>::Add(const Key& key, const Data& entry)
{
    auto position = Contains(key, entry);
    if(position == nullptr)
    {
        numberOfEntries++;
        data[HashFunction(key)].push_back(entry);
        return &(data[HashFunction(key)].back());
    }
    else
        return position;
}

template <typename Key, typename Data>
const Data* HashTable<Key, Data>::Contains(const Key &key, const Data &entry) const
{
    auto i = HashFunction(key);
    if(data[i].empty())
    {
        return nullptr;
    }
    auto result = std::find(data[i].begin(), data[i].end(), entry);
    if(result != data[i].end())
        return &(*result);
    else
        return nullptr;
}

template <typename Key, typename Data>
void HashTable<Key, Data>::Delete(const Key& key, const Data& entry)
{
    if(Contains(key, entry))
    {
        numberOfEntries--;
        std::vector<Data>& v = data[HashFunction(key)];
        v.erase(std::remove(v.begin(), v.end(), entry), v.end());
    }
}

template <typename Key, typename Data>
const std::vector<Data> &HashTable<Key, Data>::Get(const Key& key) const
{
    if(data[HashFunction(key)].empty())
    {
        throw HashTableEntryNotFoundException("failed to get() entry");
    }
    return data[HashFunction(key)];
}

template <typename Key, typename Data>
std::vector<Data> &HashTable<Key, Data>::Get(const Key& key)
{
    if(data[HashFunction(key)].empty())
    {
        throw HashTableEntryNotFoundException("failed to get() entry");
    }
    return data[HashFunction(key)];
}

template <typename Key, typename Data>
const Data& HashTable<Key, Data>::GetEntry(const Key& key, const Data& entry) const
{
    const std::vector<Data>& tmp = Get(key);
    if (!Contains(key, entry))
    {
        throw HashTableEntryNotFoundException("failed to getEntry()");
    }
    return *std::find(tmp.begin(), tmp.end(), entry);
}

template <typename Key, typename Data>
Data& HashTable<Key, Data>::GetEntry(const Key& key, const Data& entry)
{
    std::vector<Data>& tmp = Get(key);
    if (!Contains(key, entry))
    {
        throw HashTableEntryNotFoundException("failed to getEntry()");
    }
    return *std::find(tmp.begin(), tmp.end(), entry);
}

template <typename Key, typename Data>
unsigned long HashTable<Key, Data>::HashFunction(const Key& key) const
{
    std::hash<Key> hash;
    return hash(key) % size;
}

template <typename Key, typename Data>
void HashTable<Key, Data>::WriteToFile(std::ofstream& file) const
{
    size_t sz = data.size();
    file.write(reinterpret_cast<const char*>(&size), sizeof(size));
    file.write(reinterpret_cast<const char*>(&numberOfEntries), sizeof(numberOfEntries));
    file.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    for(size_t i = 0; i < sz; i++)
    {
        size_t tmpSize = data[i].size();
        file.write(reinterpret_cast<const char*>(&tmpSize), sizeof(tmpSize));
        for(size_t j = 0; j < tmpSize; j++)
        {
            file << data[i][j];
        }
    }
}

template <typename Key, typename Data>
void HashTable<Key, Data>::ReadFromFile(std::ifstream& file)
{
    data.clear();
    size_t sz;
    file.read(reinterpret_cast<char*>(&size), sizeof(size));
    file.read(reinterpret_cast<char*>(&numberOfEntries), sizeof(numberOfEntries));
    file.read(reinterpret_cast<char*>(&sz), sizeof(sz));
    data.resize(sz);
    for(size_t i = 0; i < sz; i++)
    {
        size_t tmpSize;
        file.read(reinterpret_cast<char*>(&tmpSize), sizeof(tmpSize));
        data[i].resize(tmpSize);
        for(size_t j = 0; j < tmpSize; j++)
        {
            file >> data[i][j];
        }
    }
}
