#ifndef NSNBCLASSIFICATOR_H
#define NSNBCLASSIFICATOR_H

#include "classifier.h"
#include <set>
#include <exception>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT NSNBClassifier : public Classifier
{
public:
    NSNBClassifier(const std::string& _dictionaryName = defaultDictionaryName, FeatureExtractor* _featureExtractor = nullptr,
                   double _threshold = defaultThreshold, double _epsilon = defaultEpsilon, double _delta = defaultDelta,
                   double _alpha = defaultAlpha, double _beta = defaultBeta, unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : Classifier(_dictionaryName, _featureExtractor, _threshold), epsilon(_epsilon), delta(_delta),
          alpha(_alpha), beta(_beta), numberOfFeaturesToConsider(_numberOfFeaturesToConsider) { }
    NSNBClassifier(FeatureExtractor* _featureExtractor = nullptr, double _threshold = defaultThreshold, double _epsilon = defaultEpsilon, double _delta = defaultDelta,
                   double _alpha = defaultAlpha, double _beta = defaultBeta, unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : Classifier(_featureExtractor, _threshold), epsilon(_epsilon), delta(_delta),
          alpha(_alpha), beta(_beta), numberOfFeaturesToConsider(_numberOfFeaturesToConsider) { }
    virtual ~NSNBClassifier() { SaveDictionary(); }
    ClassificationResult Classify(const std::string& text, const ClassificationResult expectedResult = ClassificationResult::none);
    virtual void SwitchDictionary(const std::string& _dictionaryName);

    static const double defaultThreshold;
    static const double defaultEpsilon;
    static const double defaultDelta;
    static const double defaultAlpha;
    static const double defaultBeta;
    static const std::string defaultDictionaryName;
    static const unsigned long defaultNumberOfFeaturesToConsider;
    static const unsigned long maxNumberOfIterations;
    void DeleteWeakFeatures();

protected:
    virtual void LoadDictionary(const std::string& _dictionaryName);
    virtual void SaveDictionary();
    virtual void Train(const std::list<Feature> &features, const ClassificationResult expectedResut);
    virtual ClassificationResult Classify(const std::list<Feature>& features, double &criteria, const double epsilon = defaultEpsilon, bool createNewEntry = false);

    double epsilon;
    double delta;
    double alpha;
    double beta;
    unsigned long numberOfFeaturesToConsider;
};

class CLASSIFIERSHARED_EXPORT modNSNBClassifier : public NSNBClassifier
{
public:
    modNSNBClassifier(const std::string& _dictionaryName = defaultDictionaryName, FeatureExtractor* _featureExtractor = nullptr,
                   double _threshold = defaultThreshold, double _epsilon = defaultEpsilon, double _delta = defaultDelta,
                   double _alpha = defaultAlpha, double _beta = defaultBeta, unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : NSNBClassifier(_dictionaryName, _featureExtractor, _threshold, _epsilon, _delta, _alpha, _beta, _numberOfFeaturesToConsider){ }
    modNSNBClassifier(FeatureExtractor* _featureExtractor = nullptr, double _threshold = defaultThreshold, double _epsilon = defaultEpsilon, double _delta = defaultDelta,
                   double _alpha = defaultAlpha, double _beta = defaultBeta, unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : NSNBClassifier(_featureExtractor, _threshold, _epsilon, _delta, _alpha, _beta, _numberOfFeaturesToConsider) { }
    virtual ~modNSNBClassifier() { }

protected:
    virtual void Train(const std::list<Feature> &features, const ClassificationResult expectedResut);
};

#endif // NSNBCLASSIFICATOR_H
