#ifndef CHARACTERNGRAMMFEATUREEXTRACTOR_H
#define CHARACTERNGRAMMFEATUREEXTRACTOR_H

#include "featureextractor.h"
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT CharacterNGrammFeatureExtractor : public FeatureExtractor
{
public:
    CharacterNGrammFeatureExtractor() {}
    CharacterNGrammFeatureExtractor(unsigned int _n) : k(_n) {}
    virtual ~CharacterNGrammFeatureExtractor() {}
    virtual std::list<Feature> Extract(const std::string& text) const;
    virtual bool isWeighted() const { return false; }
    virtual std::string Signature() { return "CNG" + std::to_string(k); }
    unsigned long k = 5;
};

#endif // CHARACTERNGRAMMFEATUREEXTRACTOR_H
