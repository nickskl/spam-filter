#include"nsnbClassifier.h"
#include<map>

const double NSNBClassifier::defaultThreshold = 0.5;
const double NSNBClassifier::defaultDelta = 0.2;
const double NSNBClassifier::defaultEpsilon = 1E-5;
const double NSNBClassifier::defaultAlpha = 0.5;
const double NSNBClassifier::defaultBeta = 0.5;
const std::string NSNBClassifier::defaultDictionaryName = "dictionary.dct";
const unsigned long NSNBClassifier::defaultNumberOfFeaturesToConsider = -1;
const unsigned long NSNBClassifier::maxNumberOfIterations = 512;

ClassificationResult NSNBClassifier::Classify(const std::string& text, const ClassificationResult expectedResult)
{
    if (featureExtractor == nullptr)
    {
        throw ClassifierNoFeatureExtractorSelectedException();
    }
    auto features = featureExtractor->Extract(text);
    double criteria;
    bool training = false;

    if(expectedResult != ClassificationResult::none)
    {
        training = true;
    }

    if (!needToSaveDictionary)
    {
        return Classify(features, criteria);
    }

    ClassificationResult result;

    if(training)
    {
        result = Classify(features, criteria, defaultEpsilon, true);
        unsigned int numberOfIterations = 0;
        while((numberOfIterations < maxNumberOfIterations)&&
              ((result != expectedResult)||
               ((criteria < (threshold+delta))&&(criteria > (threshold-delta)))))
        {
            Train(features, expectedResult);
            result = Classify(features, criteria);
            numberOfIterations++;
        }
        if((expectedResult == ClassificationResult::spam))
            dictionary.IncSpam();
        else
            dictionary.IncHam();
    }
    else
        result = Classify(features, criteria);
    return result;
}

void NSNBClassifier::SwitchDictionary(const std::string& _dictionaryName)
{
    SaveDictionary();
    LoadDictionary(_dictionaryName);
}

void NSNBClassifier::LoadDictionary(const std::string& _dictionaryName)
{
    dictionary.LoadFromFile(_dictionaryName);
    dictionaryName = _dictionaryName;
}

void NSNBClassifier::SaveDictionary()
{
    dictionary.SaveToFile(dictionaryName);
}

void NSNBClassifier::Train(const std::list<Feature>& features, const ClassificationResult expectedResut)
{
    if((expectedResut == ClassificationResult::spam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                f->ChangeConfidence(1/beta);
                f->IncSpam();
            }
        }
    }
    else if((expectedResut == ClassificationResult::nonSpam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                f->ChangeConfidence(alpha);
                f->IncHam();
            }
        }
    }
}

ClassificationResult NSNBClassifier::Classify(const std::list<Feature> &features, double& criteria, const double epsilon, bool createNewEntry)
{
    double totalProb = 0;
    unsigned long count = 0;
    double maxWeight = 1;
    if(featureExtractor->isWeighted())
    {
        maxWeight = 4<<(featureExtractor->k);
    }
    std::multiset<double> probSet;

    for(auto feature = features.begin(); feature != features.end(); feature++)
    {
        const DictionaryEntry* entry = dictionary.GetEntry(feature->GetData());
        if(entry == nullptr)
        {
            if(!createNewEntry)
                continue;
            entry = dictionary.CreateEntry(feature->GetData());
            if(!entry)
                continue;
        }

        double prob = (entry->SpamCount() + epsilon)/(entry->HamCount() + epsilon)*entry->ConfidenceFactor();
        if(featureExtractor->isWeighted())
            prob *= feature->GetWeight()/(double)maxWeight;
        probSet.insert(std::log(prob));
    }

    for(auto prob = probSet.rbegin(); (count != numberOfFeaturesToConsider)&&(prob != probSet.rend()); count++, prob++)
    {
        totalProb += *prob;
    }

    totalProb += (features.size()-1)*log(((double)dictionary.GetTotalHamCount()+epsilon)/(dictionary.GetTotalSpamCount() + epsilon));

    criteria = 1.0 / (1.0 + std::exp(-totalProb));

    if(criteria > threshold)
        return ClassificationResult::spam;
    else
        return ClassificationResult::nonSpam;
}

void NSNBClassifier::DeleteWeakFeatures()
{
    dictionary.DeleteWeakFeatures();
}

void modNSNBClassifier::Train(const std::list<Feature>& features, const ClassificationResult expectedResut)
{
    auto lowerBound = threshold - delta;
    auto upperBound = threshold + delta;
    unsigned long maxWeight = 1;
    if(featureExtractor->isWeighted())
    {
        maxWeight = 4<<(featureExtractor->k);
    }

    if((expectedResut == ClassificationResult::spam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                double prob = (f->SpamCount() + epsilon)*f->ConfidenceFactor()/(f->SpamCount()*f->ConfidenceFactor() + f->HamCount() + epsilon);
                if(featureExtractor->isWeighted())
                {
                    prob *= feature->GetWeight();
                    prob /= maxWeight;
                }
                prob /= upperBound;
                if(prob < 1)
                    f->ChangeConfidence(1/(beta*(1 - prob)));
                f->IncSpam();
            }
        }
    }
    else if((expectedResut == ClassificationResult::nonSpam))
    {
        for(auto feature = features.begin(); feature != features.end(); feature++)
        {
            auto f = dictionary.GetEntry(feature->GetData());
            if(f != nullptr)
            {
                double prob = (f->SpamCount() + epsilon)*f->ConfidenceFactor()/(f->SpamCount()*f->ConfidenceFactor() + f->HamCount() + epsilon);
                if(featureExtractor->isWeighted())
                {
                    prob *= feature->GetWeight();
                    prob /= maxWeight;
                }
                prob = lowerBound / prob;
                if(prob < 1)
                    f->ChangeConfidence(alpha*(1 - prob));
                f->IncHam();
            }
        }
    }
}
