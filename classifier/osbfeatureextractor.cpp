#include "osbfeatureextractor.h"

std::list<Feature> OSBFeatureExtractor::Extract(const std::string &text) const
{
    std::list<Feature> result;
    bool done = false;
    const char * position = text.data();
    while(!done)
    {
        std::string tmp = ExtractWord(position);
        const char * secondWord = position;
        for(unsigned i = 0; i < k; i++)
        {
            if(*secondWord != 0)
            {
                std::string buffer;
                for(unsigned j = 0; j < i; j++)
                    buffer.push_back(wildcard);
                buffer += ExtractWord(secondWord);
                result.push_back(tmp + " " + buffer);
            }
        }
        if(*position == '\0')
            done = true;
    }
    return result;
}

std::string OSBFeatureExtractor::ExtractWord(const char *&text) const
{
    std::string result;

    while(*text == ' ')
        text++;
    const char* start = text;
    while((*text != '\0')&&(*text != ' '))
        text++;
    result.append(start, text);
    return result;
}
