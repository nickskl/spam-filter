#include "characterngrammfeatureextractor.h"

std::list<Feature> CharacterNGrammFeatureExtractor::Extract(const std::string& text) const
{
    std::list<Feature> result;
    const char * position = text.data();
    unsigned int length = text.length();

    while(length >= k)
    {
        result.push_back(std::string(position, position+k));
        length -= 1;
        position += 1;
    }
    return result;
}
