#ifndef FEATURE_H
#define FEATURE_H

#include <string>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT Feature
{
public:
    Feature(std::string _data, unsigned long _weight = 1);

    const std::string &GetData() const;
    std::string &GetData();
    void SetData(const std::string& _data);

    unsigned long GetWeight() const;

    bool operator==(const Feature& rhs) const { return data == rhs.data; }
private:
    std::string data;
    unsigned long weight;
};

#endif // FEATURE_H
