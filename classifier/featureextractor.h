#ifndef FEATUREEXTRACTOR_H
#define FEATUREEXTRACTOR_H

#include "feature.h"
#include <list>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT FeatureExtractor
{
public:
    virtual ~FeatureExtractor() {}
    virtual std::list<Feature> Extract(const std::string& text) const = 0;
    virtual bool isWeighted() const = 0;
    virtual std::string Signature() = 0;
    unsigned long k;
};

#endif // FEATUREEXTRACTOR_H
