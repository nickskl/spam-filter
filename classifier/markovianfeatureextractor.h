#ifndef MARKOVIANFEATUREEXTRACTOR_H
#define MARKOVIANFEATUREEXTRACTOR_H

#include "osbfeatureextractor.h"
#include <vector>
#include <cmath>
#include"classifier_global.h"

class CLASSIFIERSHARED_EXPORT MarkovianFeatureExtractor : public OSBFeatureExtractor
{
public:
    MarkovianFeatureExtractor() {}
    MarkovianFeatureExtractor(int _k) : k(_k) {}
    virtual ~MarkovianFeatureExtractor() {}
    virtual std::list<Feature> Extract(const std::string& text) const;
    virtual bool isWeighted() const { return true; }
    virtual std::string Signature() { return "MFE" + std::to_string(k); }
    unsigned long k = 5;
private:
    void ExtractFeatures(std::list<Feature>& result, const char* text, unsigned int wordsCount, unsigned long weight = 4) const;
    static const char wildcard;
};

#endif // MARKOVIANFEATUREEXTRACTOR_H
