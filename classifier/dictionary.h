#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <limits>
#include <string>
#include "hash.h"
#include <fstream>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT DictionaryException : public std::exception
{
public:
    DictionaryException(const std::string& _buffer) : buffer(_buffer) {}
    const char* what() const noexcept { return buffer.c_str();}
protected:
    const std::string buffer;
};

class CLASSIFIERSHARED_EXPORT DictionaryFileNotFoundException : public DictionaryException
{
public:
    DictionaryFileNotFoundException(const std::string& _buffer) : DictionaryException(_buffer) {}
};


class CLASSIFIERSHARED_EXPORT DictionaryEntry
{
public:
    DictionaryEntry() : spamCount(0), hamCount(0), confidenceFactor(1.0), key("") {}
    DictionaryEntry(const std::string& _key) : spamCount(0), hamCount(0), confidenceFactor(1.0), key(_key) {}

    unsigned long long SpamCount() const { return spamCount; }
    unsigned long long HamCount() const { return hamCount; }
    double ConfidenceFactor() const { return confidenceFactor; }
    const std::string& Key() const { return key; }

    void IncSpam() { if (spamCount != ULLONG_MAX) spamCount++; }
    void DecSpam() { if (spamCount != 0) spamCount--; }
    void IncHam() { if (hamCount != ULLONG_MAX) hamCount++; }
    void DecHam() { if (hamCount != 0) hamCount--; }
    void ChangeConfidence(double factor);

    bool operator==(const DictionaryEntry& rhs) const { return key == rhs.key; }
    bool operator !=(const DictionaryEntry& rhs) const { return !((*this)==rhs); }

    friend std::istream& operator>>(std::istream& in, DictionaryEntry& obj);
    friend std::ostream& operator<<(std::ostream& out, const DictionaryEntry& obj);

    static const double min;
    static const double max;

protected:
    unsigned long long spamCount;
    unsigned long long hamCount;
    double confidenceFactor;
    std::string key;
};

class CLASSIFIERSHARED_EXPORT Dictionary
{
public:
    Dictionary(const std::string& _signature, const unsigned long long _maxNumberOfEntries = defaultMaxNumberOfEntries,
               const unsigned long long _maxHashTableSize = HashTable<std::string, DictionaryEntry>::defaultSize);

    DictionaryEntry* GetEntry(const std::string& key);
    const DictionaryEntry* GetEntry(const std::string& key) const;

    const DictionaryEntry *CreateEntry(const std::string& key);
    const DictionaryEntry *AddEntry(const DictionaryEntry& entry);
    void DeleteEntry(const DictionaryEntry& entry);

    void DeleteWeakFeatures();

    void CreateDictionary(const std::string &path);
    void SaveToFile(const std::string &path);
    void LoadFromFile(const std::string& path);

    void IncSpam() { if (totalSpamCount != ULLONG_MAX>>1) totalSpamCount++; }
    void DecSpam() { if (totalSpamCount != 0) totalSpamCount--; }
    void IncHam() { if (totalHamCount != ULLONG_MAX>>1) totalHamCount++; }
    void DecHam() { if (totalHamCount != 0) totalHamCount--; }

    const unsigned long long& GetTotalSpamCount() { return totalSpamCount; }
    const unsigned long long& GetTotalHamCount() { return totalHamCount; }
    const std::string& Signature() const { return signature; }
private:
    std::string signature;
    HashTable<std::string, DictionaryEntry> hashTable;
    unsigned long long totalSpamCount;
    unsigned long long totalHamCount;
    unsigned long long maxNumberOfEntries;
    static const unsigned long long defaultMaxNumberOfEntries;
};

#endif // DICTIONARY_H
