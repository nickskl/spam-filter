#ifndef CLASSIFICATOR_H
#define CLASSIFICATOR_H

#include <string>
#include "dictionary.h"
#include "featureextractor.h"
#include "classifier_global.h"

enum class ClassificationResult {none, spam, nonSpam};

class CLASSIFIERSHARED_EXPORT ClassifierException : public std::exception
{
public:
    ClassifierException(const std::string& _buffer) : buffer(_buffer) {}
    const char* what() const noexcept { return buffer.c_str();}
protected:
    const std::string buffer;
};

class CLASSIFIERSHARED_EXPORT ClassifierNoFeatureExtractorSelectedException : public ClassifierException
{
public:
    ClassifierNoFeatureExtractorSelectedException() : ClassifierException("passing nullptr as FeatureExtractor pointer") {}
};


class CLASSIFIERSHARED_EXPORT Classifier
{
public:
    Classifier(const std::string& _dictionaryName, FeatureExtractor* _featureExtractor, double _threshold)
        : threshold(_threshold), dictionaryName(_dictionaryName), featureExtractor(_featureExtractor), dictionary(Dictionary("DCT" + featureExtractor->Signature())), needToSaveDictionary(true)
    {
        dictionary.LoadFromFile(_dictionaryName);
    }

    Classifier(FeatureExtractor* _featureExtractor, double _threshold)
        : threshold(_threshold), featureExtractor(_featureExtractor), dictionary(Dictionary("DCT" + featureExtractor->Signature())), needToSaveDictionary(true) { }

    virtual ~Classifier() { if (featureExtractor != nullptr) delete featureExtractor; }

    virtual ClassificationResult Classify(const std::string& text, const ClassificationResult expectedResult = ClassificationResult::none) = 0;
    virtual void SwitchDictionary(const std::string& _dictionaryName) = 0;
    Dictionary& GetDictionary() { return dictionary; }
    const std::string& GetDictionaryName() const { return dictionaryName; }
    double& Threshold() { return threshold; }
    virtual void DeleteWeakFeatures() = 0;
    void SetSaveDictionary(bool value) { needToSaveDictionary = value; }

protected:
    virtual void LoadDictionary(const std::string& _dictionaryName) = 0;
    virtual void SaveDictionary() = 0;
    virtual void Train(const std::list<Feature>& features, const ClassificationResult expectedResut) = 0;

    double threshold;
    std::string dictionaryName;
    FeatureExtractor* featureExtractor;
    Dictionary dictionary;
    bool needToSaveDictionary;
};

#endif // CLASSIFICATOR_H
