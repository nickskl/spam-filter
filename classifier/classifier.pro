#-------------------------------------------------
#
# Project created by QtCreator 2017-04-16T21:04:00
#
#-------------------------------------------------

QT       -= gui

TARGET = classifier
TEMPLATE = lib

QMAKE_CXXFLAGS += -std=c++11 -ggdb

DEFINES += CLASSIFIER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += characterngrammfeatureextractor.cpp \
    dictionary.cpp \
    feature.cpp \
    markovianfeatureextractor.cpp \
    nsnbclassifier.cpp \
    osbfeatureextractor.cpp \
    nbclassifier.cpp

HEADERS += classifier.h\
        classifier_global.h \
    characterngrammfeatureextractor.h \
    dictionary.h \
    feature.h \
    featureextractor.h \
    hash.h \
    hash.hpp \
    markovianfeatureextractor.h \
    nsnbclassifier.h \
    osbfeatureextractor.h \
    nbclassifier.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
