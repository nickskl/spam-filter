#ifndef OSBFEATUREEXTRACTOR_H
#define OSBFEATUREEXTRACTOR_H

#include "featureextractor.h"
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT OSBFeatureExtractor : public FeatureExtractor
{
public:
    OSBFeatureExtractor() {}
    OSBFeatureExtractor(int _k) : k(_k) {}
    virtual ~OSBFeatureExtractor() {}
    virtual std::list<Feature> Extract(const std::string& text) const;
    virtual bool isWeighted() const { return false; }
    virtual std::string Signature() { return "OSB" + std::to_string(k); }
    unsigned long k = 5;
protected:
    std::string ExtractWord(const char *&text) const;
private:
    static const char wildcard = 0;
};

#endif // OSBFEATUREEXTRACTOR_H
