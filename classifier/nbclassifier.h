#ifndef NBCLASSIFIER_H
#define NBCLASSIFIER_H

#include "classifier.h"
#include <set>
#include <exception>
#include "classifier_global.h"

class CLASSIFIERSHARED_EXPORT NBClassifier : public Classifier
{
public:
    NBClassifier(const std::string& _dictionaryName = defaultDictionaryName, FeatureExtractor* _featureExtractor = nullptr,
                 double _threshold = defaultThreshold, double _epsilon = defaultEpsilon,
                 unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : Classifier(_dictionaryName, _featureExtractor, _threshold), epsilon(_epsilon), numberOfFeaturesToConsider(_numberOfFeaturesToConsider) { }
    NBClassifier(FeatureExtractor* _featureExtractor = nullptr, double _threshold = defaultThreshold, double _epsilon = defaultEpsilon,
                 unsigned long _numberOfFeaturesToConsider = defaultNumberOfFeaturesToConsider)
        : Classifier(_featureExtractor, _threshold), epsilon(_epsilon), numberOfFeaturesToConsider(_numberOfFeaturesToConsider) { }
    virtual ~NBClassifier() { SaveDictionary(); }
    ClassificationResult Classify(const std::string& text, const ClassificationResult expectedResult = ClassificationResult::none);
    virtual void SwitchDictionary(const std::string& _dictionaryName);

    static const double defaultThreshold;
    static const double defaultEpsilon;
    static const std::string defaultDictionaryName;
    static const unsigned long defaultNumberOfFeaturesToConsider;
    static const unsigned long maxNumberOfIterations;
    void DeleteWeakFeatures();

protected:
    virtual void LoadDictionary(const std::string& _dictionaryName);
    virtual void SaveDictionary();
    virtual void Train(const std::list<Feature>& features, const ClassificationResult expectedResut);
    virtual ClassificationResult Classify(const std::list<Feature>& features, double &criteria, const double epsilon = defaultEpsilon, bool createNewEntry = false);

    double epsilon;
    unsigned long numberOfFeaturesToConsider;
};

#endif // NBCLASSIFIER_H
