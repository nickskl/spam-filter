#include "markovianfeatureextractor.h"

const char MarkovianFeatureExtractor::wildcard = 0;

std::list<Feature> MarkovianFeatureExtractor::Extract(const std::string &text) const
{
    std::list<Feature> result;
    bool done = false;
    const char * position = text.data();

    while(!done)
    {
        ExtractFeatures(result, position, k);
        ExtractWord(position);
        if(*position == '\0')
            done = true;
    }
    return result;
}


void MarkovianFeatureExtractor::ExtractFeatures(std::list<Feature>& result, const char* text, unsigned int wordsCount, unsigned long weight) const
{
    std::string currentFeature = "";
    for (size_t i = 0; i < wordsCount; i++)
    {
        if (text[0] == '\0')
            return;
        currentFeature += ExtractWord(text);
        result.push_back(Feature(currentFeature, weight));
        currentFeature += " ";
        weight *= 4;
    }
}
