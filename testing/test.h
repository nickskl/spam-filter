#ifndef TEST_H
#define TEST_H

#include <string>
#include <exception>
#include <iostream>

class FailedTest : public std::exception
{
public:
    FailedTest(const std::string& _buffer) : buffer(_buffer) {}
    const char* what() const noexcept { return buffer.c_str();}
private:
    const std::string buffer;
};

class Test
{
public:
    Test(const std::string& _prefix) : prefix(_prefix) {}
    virtual ~Test() {}
    virtual void test() = 0;
protected:
    std::string prefix;
};

#endif // TEST_H
