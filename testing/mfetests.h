#ifndef MFETESTS_H
#define MFETESTS_H

#include "featureextractortests.h"
#include "markovianfeatureextractor.h"

class MFETest : public FeatureExtractorTest
{
public:
    MFETest() : FeatureExtractorTest(" MFE: ") {}
    virtual ~MFETest() {}
    virtual void test() = 0;
};


class MFEStringTest : public MFETest
{
public:
    MFEStringTest(const std::string& _text = "", unsigned int _numberOfWords = 0, int _k = 3)
        : text(_text), numberOfWords(_numberOfWords), k(_k) {}
    virtual ~MFEStringTest() {}
    virtual void test()
    {
        if ((int)numberOfWords < k)
            k = numberOfWords;

        int expectedValue = 0;

        int n = numberOfWords - k + 1;
        if(k <= 0)
            n = 0;
        if(n > 0)
            expectedValue += n*k;
        expectedValue += (k-1)*k/2;

        MarkovianFeatureExtractor extractor(k);

        if (FeatureExtractorAsserts::TestIfNumberOfFeaturesIsEqual(text, &extractor, expectedValue))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text <<
                         std::endl << "number of words = " << numberOfWords <<
                         std::endl << "k = " << k << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    std::string text;
    unsigned int numberOfWords;
    int k;
};



#endif // MFETESTS_H
