#ifndef FEATUREEXTRACTORTESTS_H
#define FEATUREEXTRACTORTESTS_H

#include <list>
#include "test.h"
#include "featureextractor.h"

class FeatureExtractorTest : public Test
{
public:
    FeatureExtractorTest(const std::string& testPrefix) : Test(" [Feature extractor test]" + testPrefix) {}
    virtual ~FeatureExtractorTest() {}
    virtual void test() = 0;
};

class FeatureExtractorAsserts
{
public:
    static bool TestIfNumberOfFeaturesIsEqual(const std::string& text, FeatureExtractor* extractor, unsigned int expectedNumberOfFeatures)
    {
        return extractor->Extract(text).size() == expectedNumberOfFeatures;
    }
    static bool TestIfFeatureListMatches(std::string& text, FeatureExtractor* extractor, std::list<Feature> expectedFeatures)
    {
        return extractor->Extract(text) == expectedFeatures;
    }
};
#endif // FEATUREEXTRACTORTESTS_H
