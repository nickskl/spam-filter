#ifndef CNGTESTS_H
#define CNGTESTS_H

#include "featureextractortests.h"
#include "characterngrammfeatureextractor.h"

class CNGTest : public FeatureExtractorTest
{
public:
    CNGTest() : FeatureExtractorTest(" CNG: ") {}
    virtual ~CNGTest() {}
    virtual void test() = 0;
};

class CNGStringTest : public CNGTest
{
public:
    CNGStringTest(const std::string& _text = "", unsigned int _n = 3)
        : text(_text), n(_n) {}
    virtual ~CNGStringTest() {}
    virtual void test()
    {
        int expectedValue = text.length() - n + 1;
        if(expectedValue < 0)
            expectedValue = 0;
        CharacterNGrammFeatureExtractor extractor(n);

        if (FeatureExtractorAsserts::TestIfNumberOfFeaturesIsEqual(text, &extractor, expectedValue))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text <<
                         std::endl << "n = " << n << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    std::string text;
    unsigned int n;
};
#endif // CNGTESTS_H
