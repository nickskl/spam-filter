#ifndef HASHTABLETESTS_H
#define HASHTABLETESTS_H

#include"test.h"
#include"hash.h"

class HashTableTest : public Test
{
public:
    HashTableTest(const std::string& testPrefix) : Test(" [Hash table test]" + testPrefix) {}
    virtual ~HashTableTest() {}
    virtual void test() = 0;
};

template <typename Key, typename Data>
class HashTableAsserts
{
public:
    static bool TestIfEntryExistsSimple(const Key& key, const HashTable<Key, Data>& hashTable)
    {
        try
        {
            hashTable.Get(key);
            return true;
        }
        catch(HashTableEntryNotFoundException& e)
        {
            return false;
        }
    }

    static bool TestIfEntryNotExistSimple(const Key& key, const HashTable<Key, Data>& hashTable)
    {
        return !TestIfEntryExistsSimple(key, hashTable);
    }

    static bool TestIfEntryExists(const Key& key, const Data& entry, const HashTable<Key, Data>& hashTable)
    {
        try
        {
            hashTable.GetEntry(key, entry);
            return true;
        }
        catch(HashTableEntryNotFoundException& e)
        {
            return false;
        }
    }

    static bool TestIfEntryNotExist(const Key& key, const Data& entry, const HashTable<Key, Data>& hashTable)
    {
        return !TestIfEntryExists(key, entry, hashTable);
    }

    static bool TestNumberOfEntriesMatches(const HashTable<Key, Data>& hashTable, unsigned long long expectedValue)
    {
        return hashTable.NumberOfEntries() == expectedValue;
    }
};


class HashTableNumberOfEntriesTest : public HashTableTest
{
public:
    HashTableNumberOfEntriesTest() : HashTableTest(" number of entries: ") {}
    virtual ~HashTableNumberOfEntriesTest() {}
    virtual void test() = 0;
};

class HashTableNumberOfEntriesSimpleTest : public HashTableNumberOfEntriesTest
{
public:
    HashTableNumberOfEntriesSimpleTest(int _expectedNoE) : expectedNoE(_expectedNoE) {}
    ~HashTableNumberOfEntriesSimpleTest() {}
    virtual void test()
    {
        HashTable<int, int> hashTable;
        for(int i = 0 ; i < expectedNoE; i++)
        {
            hashTable.Add(i, i);
        }

        if (HashTableAsserts<int, int>::TestNumberOfEntriesMatches(hashTable, expectedNoE))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "expected number of entries = " <<
                         expectedNoE << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int expectedNoE;
};

class HashTableNumberOfEntriesWithDuplicatesTest : public HashTableNumberOfEntriesTest
{
public:
    HashTableNumberOfEntriesWithDuplicatesTest(int _expectedNoE, int _numberOfDups)
        : expectedNoE(_expectedNoE), numberOfDups(_numberOfDups) {}
    ~HashTableNumberOfEntriesWithDuplicatesTest() {}
    virtual void test()
    {
        HashTable<int, int> hashTable;
        for(int i = 0 ; i < expectedNoE; i++)
        {
            hashTable.Add(i, i);
        }

        for(int i = 0; i < numberOfDups; i++)
        {
            hashTable.Add(expectedNoE/2, expectedNoE/2);
        }


        if (HashTableAsserts<int, int>::TestNumberOfEntriesMatches(hashTable, expectedNoE))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "expected number of entries = " <<
                         expectedNoE << std::endl << "number of duplications = " << numberOfDups << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int expectedNoE;
    int numberOfDups;
};

class HashTableAddTest : public HashTableTest
{
public:
    HashTableAddTest() : HashTableTest(" add: ") {}
    virtual ~HashTableAddTest() {}
    virtual void test() = 0;
};


class HashTableAddSimpleTest : public HashTableAddTest
{
public:
    HashTableAddSimpleTest(int _key, int _entry) : key(_key), entry(_entry) {}
    virtual ~HashTableAddSimpleTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        hashTable.Add(key, entry);


        if (HashTableAsserts<int, int>::TestIfEntryExistsSimple(key, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "entry = " << entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int entry;
};

class HashTableAddNormalTest : public HashTableAddTest
{
public:
    HashTableAddNormalTest(int _key, int _entry) : key(_key), entry(_entry) {}
    virtual ~HashTableAddNormalTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        hashTable.Add(key, entry);


        if (HashTableAsserts<int, int>::TestIfEntryExists(key, entry, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "entry = " << entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int entry;
};

class HashTableAddCollisionTest : public HashTableAddTest
{
public:
    HashTableAddCollisionTest(int _key, int _numberOfCollisions) : key(_key), numberOfCollisions(_numberOfCollisions) {}
    virtual ~HashTableAddCollisionTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        for(int i = 0; i < numberOfCollisions; i++)
        {
            hashTable.Add(key, i);
        }



        if (HashTableAsserts<int, int>::TestIfEntryExists(key, numberOfCollisions/2, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "number of collisions = " << numberOfCollisions << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int numberOfCollisions;
};

class HashTableAddCollisionElementNotExistTest : public HashTableAddTest
{
public:
    HashTableAddCollisionElementNotExistTest(int _key, int _numberOfCollisions) : key(_key), numberOfCollisions(_numberOfCollisions) {}
    virtual ~HashTableAddCollisionElementNotExistTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        for(int i = 0; i < numberOfCollisions; i++)
        {
            hashTable.Add(key, i);
        }



        if (HashTableAsserts<int, int>::TestIfEntryNotExist(key, numberOfCollisions+1, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "number of collisions = " << numberOfCollisions << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int numberOfCollisions;
};

class HashTableDeleteTest : public HashTableTest
{
public:
    HashTableDeleteTest() : HashTableTest(" delete: ") {}
    virtual ~HashTableDeleteTest() {}
    virtual void test() = 0;
};


class HashTableDeleteSimpleTest : public HashTableDeleteTest
{
public:
    HashTableDeleteSimpleTest(int _key, int _entry) : key(_key), entry(_entry) {}
    virtual ~HashTableDeleteSimpleTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        hashTable.Add(key, entry);


        hashTable.Delete(key, entry);

        if (HashTableAsserts<int, int>::TestIfEntryNotExistSimple(key, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "entry = " << entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int entry;
};

class HashTableDeleteNormalTest : public HashTableDeleteTest
{
public:
    HashTableDeleteNormalTest(int _key, int _entry) : key(_key), entry(_entry) {}
    virtual ~HashTableDeleteNormalTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        hashTable.Add(key, entry);


        hashTable.Delete(key, entry);

        if (HashTableAsserts<int, int>::TestIfEntryNotExist(key, entry, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "entry = " << entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int entry;
};

class HashTableDeleteCollisionTest : public HashTableDeleteTest
{
public:
    HashTableDeleteCollisionTest(int _key, int _numberOfCollisions) : key(_key), numberOfCollisions(_numberOfCollisions) {}
    virtual ~HashTableDeleteCollisionTest() {}
    void test()
    {
        HashTable<int, int> hashTable;
        for(int i = 0; i < numberOfCollisions; i++)
        {
            hashTable.Add(key, i);
        }


        hashTable.Delete(key, numberOfCollisions/2);

        if (HashTableAsserts<int, int>::TestIfEntryNotExist(key, numberOfCollisions/2, hashTable))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "key = " <<
                         key << std::endl << "number of collisions = " << numberOfCollisions << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int key;
    int numberOfCollisions;
};

class HashTableRWTest : public HashTableTest
{
public:
    HashTableRWTest() : HashTableTest(" read/write: ") {}
    virtual ~HashTableRWTest() {}
    virtual void test() = 0;
};

class HashTableRWSimpleTest : public HashTableRWTest
{
public:
    HashTableRWSimpleTest(int _NoE, int _size) : NoE(_NoE), size(_size) {}
    ~HashTableRWSimpleTest() {}
    virtual void test()
    {
        HashTable<int, std::string> hashTable(size);
        for(int i = 0 ; i < NoE; i++)
        {
            hashTable.Add(i, std::to_string(i));
        }

        std::ofstream outfile;
        outfile.open("testfile.bin", std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
        hashTable.WriteToFile(outfile);
        outfile.close();

        std::ifstream file;
        file.open("testfile.bin", std::ios_base::in | std::ios_base::binary);
        hashTable.ReadFromFile(file);
        file.close();

        if (HashTableAsserts<int, std::string>::TestNumberOfEntriesMatches(hashTable, NoE))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "expected number of entries = " <<
                         NoE << std::endl << " size = " << size << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int NoE;
    int size;
};

#endif // HASHTABLETESTS_H
