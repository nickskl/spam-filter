#ifndef DICTIONARYTESTS_H
#define DICTIONARYTESTS_H

#include "test.h"
#include "dictionary.h"

class DictionaryTest : public Test
{
public:
    DictionaryTest(const std::string& testPrefix) : Test(" [Dictionary]" + testPrefix) {}
    virtual ~DictionaryTest() {}
    virtual void test() = 0;
};

class DictionaryAsserts
{
public:
    static bool TestIfEntryExists(const Dictionary& dictionary, const std::string& entry)
    {
        return dictionary.GetEntry(entry) != nullptr;
    }

    static bool TestIfEntryNotExist(const Dictionary& dictionary, const std::string& entry)
    {
        return dictionary.GetEntry(entry) == nullptr;
    }
};

class DictionaryAddTest : public DictionaryTest
{
public:
    DictionaryAddTest() : DictionaryTest(" add: ") {}
    virtual ~DictionaryAddTest() {}
    virtual void test() = 0;
};

class DictionaryAddSimpleTest : public DictionaryAddTest
{
public:
    DictionaryAddSimpleTest(std::string _entry) : entry(_entry) {}
    ~DictionaryAddSimpleTest() {}
    virtual void test()
    {
        Dictionary dictionary("aaa");

        dictionary.AddEntry(entry);

        if (DictionaryAsserts::TestIfEntryExists(dictionary, entry))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << " entry = " <<
                         entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    std::string entry;
};

class DictionaryDeleteTest : public DictionaryTest
{
public:
    DictionaryDeleteTest() : DictionaryTest(" delete: ") {}
    virtual ~DictionaryDeleteTest() {}
    virtual void test() = 0;
};

class DictionaryDeleteSimpleTest : public DictionaryDeleteTest
{
public:
    DictionaryDeleteSimpleTest(std::string _entry) : entry(_entry) {}
    ~DictionaryDeleteSimpleTest() {}
    virtual void test()
    {
        Dictionary dictionary("aaa");

        dictionary.AddEntry(entry);

        dictionary.DeleteEntry(entry);

        if (DictionaryAsserts::TestIfEntryNotExist(dictionary, entry))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << " entry = " <<
                         entry << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    std::string entry;
};

class DictionaryRWTest : public DictionaryTest
{
public:
    DictionaryRWTest() : DictionaryTest(" read/write: ") {}
    virtual ~DictionaryRWTest() {}
    virtual void test() = 0;
};

class DictionaryRWSimpleTest : public DictionaryRWTest
{
public:
    DictionaryRWSimpleTest(int _NoE) : NoE(_NoE) {}
    ~DictionaryRWSimpleTest() {}
    virtual void test()
    {
        Dictionary dictionary("aaa");

        for(int i = 0 ; i < NoE; i++)
        {
            dictionary.AddEntry(DictionaryEntry(std::to_string(i)));
        }

        dictionary.SaveToFile("testfile.bin");
        dictionary.LoadFromFile("testfile.bin");

        if (DictionaryAsserts::TestIfEntryExists(dictionary, std::to_string(NoE/2)))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "expected number of entries = " <<
                         NoE << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    int NoE;
};


#endif // DICTIONARYTESTS_H
