#ifndef OSBTESTS_H
#define OSBTESTS_H

#include "featureextractortests.h"
#include "osbfeatureextractor.h"

class OSBTest : public FeatureExtractorTest
{
public:
    OSBTest() : FeatureExtractorTest(" OSB: ") {}
    virtual ~OSBTest() {}
    virtual void test() = 0;
};

class OSBStringTest : public OSBTest
{
public:
    OSBStringTest(const std::string& _text = "", unsigned int _numberOfWords = 0, unsigned int _k = 3)
        : text(_text), numberOfWords(_numberOfWords), k(_k) {}
    virtual ~OSBStringTest() {}
    virtual void test()
    {
        if (numberOfWords < k)
            k = numberOfWords;
        int expectedValue = ((int)numberOfWords * 2 - (int)k - 1)*(int)k / 2;
        if(expectedValue < 0)
            expectedValue = 0;
        OSBFeatureExtractor extractor(k);

        if (FeatureExtractorAsserts::TestIfNumberOfFeaturesIsEqual(text, &extractor, expectedValue))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text <<
                         std::endl << "number of words = " << numberOfWords <<
                         std::endl << "k = " << k << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    std::string text;
    unsigned int numberOfWords;
    unsigned int k;
};

#endif // OSBTESTS_H
