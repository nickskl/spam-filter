#include "osbtests.h"
#include "cngtests.h"
#include "mfetests.h"
#include "hashtabletests.h"
#include "dictionarytests.h"
#include "classifiertests.h"
#include <fstream>

class Exception : public std::exception
{
public:
    Exception(const std::string& _buffer) : buffer(_buffer) {}
    const char* what() const noexcept { return buffer.c_str();}
private:
    const std::string buffer;
};


class TestDescription
{
public:
    static void InterpretCommand(int argc, char *argv[])
    {
        if(argc < 4)
            throw Exception("Wrong parameter count.\n");
        std::string buffer1 = argv[1];
        std::string buffer2 = argv[2];
        std::string buffer3 = argv[3];

        if(buffer1 == "-HTT")
        {
            if(buffer2 == "-NET")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 5)
                    {
                        HashTableNumberOfEntriesSimpleTest test(std::atoi(argv[4]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-dups")
                {
                    if(argc == 6)
                    {
                        HashTableNumberOfEntriesWithDuplicatesTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
            }
            else if (buffer2 == "-ADD")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 6)
                    {
                        HashTableAddSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-normal")
                {
                    if(argc == 6)
                    {
                        HashTableAddNormalTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-collision")
                {

                    if(argc == 6)
                    {
                        HashTableAddCollisionTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-ncollision")
                {
                    if(argc == 6)
                    {
                        HashTableAddCollisionElementNotExistTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
            }
            else if (buffer2 == "-DEL")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 6)
                    {
                        HashTableDeleteSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-normal")
                {
                    if(argc == 6)
                    {
                        HashTableDeleteNormalTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-collision")
                {

                    if(argc == 6)
                    {
                        HashTableDeleteCollisionTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
            }
            else if (buffer2 == "-RW")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 6)
                    {
                        HashTableRWSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
            }
        }
        else if(buffer1 == "-FET")
        {
            if(buffer2 == "-OSB")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 7)
                    {
                        std::ifstream file(argv[4]);
                        std::string text;

                        file.seekg(0, std::ios::end);
                        text.reserve(file.tellg());
                        file.seekg(0, std::ios::beg);

                        text.assign((std::istreambuf_iterator<char>(file)),
                                    std::istreambuf_iterator<char>());
                        file.close();
                        OSBStringTest test(text, std::atoi(argv[5]), std::atoi(argv[6]));
                        test.test();
                        return;
                    }
                }
            }
            else if (buffer2 == "-CNG")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 6)
                    {
                        std::ifstream file(argv[4]);
                        std::string text;

                        file.seekg(0, std::ios::end);
                        text.reserve(file.tellg());
                        file.seekg(0, std::ios::beg);

                        text.assign((std::istreambuf_iterator<char>(file)),
                                    std::istreambuf_iterator<char>());
                        file.close();
                        CNGStringTest test(text, std::atoi(argv[5]));
                        test.test();
                        return;
                    }
                }
            }
            else if(buffer2 == "-MFE")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 7)
                    {
                        std::ifstream file(argv[4]);
                        std::string text;

                        file.seekg(0, std::ios::end);
                        text.reserve(file.tellg());
                        file.seekg(0, std::ios::beg);

                        text.assign((std::istreambuf_iterator<char>(file)),
                                    std::istreambuf_iterator<char>());
                        file.close();
                        MFEStringTest test(text, std::atoi(argv[5]), std::atoi(argv[6]));
                        test.test();
                        return;
                    }
                }
            }
        }
        else if(buffer1 == "-DCT")
        {
            if(buffer2 == "-ADD")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 5)
                    {
                        std::ifstream file(argv[4]);
                        std::string text;

                        file.seekg(0, std::ios::end);
                        text.reserve(file.tellg());
                        file.seekg(0, std::ios::beg);

                        text.assign((std::istreambuf_iterator<char>(file)),
                                    std::istreambuf_iterator<char>());
                        file.close();

                        DictionaryAddSimpleTest test(text);
                        test.test();
                        return;
                    }
                }
            }
            else if(buffer2 == "-DEL")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 5)
                    {
                        std::ifstream file(argv[4]);
                        std::string text;

                        file.seekg(0, std::ios::end);
                        text.reserve(file.tellg());
                        file.seekg(0, std::ios::beg);

                        text.assign((std::istreambuf_iterator<char>(file)),
                                    std::istreambuf_iterator<char>());
                        file.close();

                        DictionaryDeleteSimpleTest test(text);
                        test.test();
                        return;
                    }
                }
            }
            else if (buffer2 == "-RW")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 5)
                    {
                        DictionaryRWSimpleTest test(std::atoi(argv[4]));
                        test.test();
                        return;
                    }
                }
            }
        }
        else if(buffer1 == "-CLS")
        {
            if(buffer2 == "-NSNB")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 7)
                    {
                        NSNBCSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-train")
                {
                    if(argc == 7)
                    {
                        NSNBCTrainSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
            }
            else if(buffer2 == "-NB")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 7)
                    {
                        NBCSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-train")
                {
                    if(argc == 7)
                    {
                        NBCTrainSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
            }
            else if(buffer2 == "-modNSNB")
            {
                if(buffer3 == "-simple")
                {
                    if(argc == 7)
                    {
                        modNSNBCSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
                else if(buffer3 == "-train")
                {
                    if(argc == 7)
                    {
                        modNSNBCTrainSimpleTest test(std::atoi(argv[4]), std::atoi(argv[5]), static_cast<ClassificationResult>(std::atoi(argv[6])));
                        test.test();
                        return;
                    }
                }
            }
        }

        throw Exception("Wrong parameter count.\n");
    }
};


int main(int argc, char *argv[])
{
    TestDescription::InterpretCommand(argc, argv);
    return 0;
}
