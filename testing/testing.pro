QT += core
QT -= gui

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

TARGET = testing
CONFIG += console
CONFIG -= app_bundle

LIBS += -L../classifier/release -lclassifier

TEMPLATE = app

INCLUDEPATH += ../classifier
DEPENDPATH += ../classifier

SOURCES += main.cpp
HEADERS += \
    test.h \
    osbtests.h \
    cngtests.h \
    hashtabletests.h \
    featureextractortests.h \
    dictionarytests.h \
    classifiertests.h \
    mfetests.h \
    ../classifier/characterngrammfeatureextractor.h \
    ../classifier/classifier.h \
    ../classifier/classifier_global.h \
    ../classifier/dictionary.h \
    ../classifier/feature.h \
    ../classifier/featureextractor.h \
    ../classifier/hash.h \
    ../classifier/hash.hpp \
    ../classifier/markovianfeatureextractor.h \
    ../classifier/nsnbclassifier.h \
    ../classifier/osbfeatureextractor.h
