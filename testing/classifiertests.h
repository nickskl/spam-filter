#ifndef CLASSIFIERTESTS_H
#define CLASSIFIERTESTS_H

#include "test.h"
#include "nsnbclassifier.h"
#include "nbclassifier.h"
#include "osbfeatureextractor.h"

class ClassifierTest : public Test
{
public:
    ClassifierTest(const std::string& testPrefix) : Test(" [Classifier]" + testPrefix) {}
    virtual ~ClassifierTest() {}
    virtual void test() = 0;
};

class ClassifierAsserts
{
public:
    static bool TestIfResultIsEqual(ClassificationResult result, ClassificationResult expectedResult)
    {
        return expectedResult == result;
    }
};

class NSNBClassifierTest : public ClassifierTest
{
public:
    NSNBClassifierTest() : ClassifierTest(" NSNB classifier: ") {}
    virtual ~NSNBClassifierTest() {}
    virtual void test() = 0;
};

class NSNBCSimpleTest : public NSNBClassifierTest
{
public:
    NSNBCSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~NSNBCSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        NSNBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        std::string text = "spam spam ham ham";

        auto result = classifier.Classify(text);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};

class NSNBCTrainSimpleTest : public NSNBClassifierTest
{
public:
    NSNBCTrainSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~NSNBCTrainSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        NSNBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();
        std::string text = "spam spam ham ham";

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        auto result = classifier.Classify(text, expectedResult);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};

class NBClassifierTest : public ClassifierTest
{
public:
    NBClassifierTest() : ClassifierTest(" NB classifier: ") {}
    virtual ~NBClassifierTest() {}
    virtual void test() = 0;
};

class NBCSimpleTest : public NBClassifierTest
{
public:
    NBCSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~NBCSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        NBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        std::string text = "spam spam ham ham";

        auto result = classifier.Classify(text);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};

class NBCTrainSimpleTest : public NBClassifierTest
{
public:
    NBCTrainSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~NBCTrainSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        NBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();
        std::string text = "spam spam ham ham";

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        auto result = classifier.Classify(text, expectedResult);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};

class modNSNBClassifierTest : public ClassifierTest
{
public:
    modNSNBClassifierTest() : ClassifierTest(" modNSNB classifier: ") {}
    virtual ~modNSNBClassifierTest() {}
    virtual void test() = 0;
};

class modNSNBCSimpleTest : public modNSNBClassifierTest
{
public:
    modNSNBCSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~modNSNBCSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        modNSNBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        std::string text = "spam spam ham ham";

        auto result = classifier.Classify(text);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};

class modNSNBCTrainSimpleTest : public NSNBClassifierTest
{
public:
    modNSNBCTrainSimpleTest(unsigned long long _spamCount, unsigned long long _hamCount, ClassificationResult _expectedResult)
        : spamCount(_spamCount), hamCount(_hamCount), expectedResult(_expectedResult) {}
    virtual ~modNSNBCTrainSimpleTest() {}
    virtual void test()
    {
        FeatureExtractor* fe = new OSBFeatureExtractor(1);
        modNSNBClassifier classifier(fe);
        Dictionary& dictionary = classifier.GetDictionary();

        DictionaryEntry realSpam("spam spam");
        for(unsigned long long i = 0; i < spamCount; i++)
            realSpam.IncSpam();
        DictionaryEntry realHam("ham ham");
        for(unsigned long long i = 0; i < hamCount; i++)
            realHam.IncHam();
        std::string text = "spam spam ham ham";

        dictionary.AddEntry(realSpam);
        dictionary.AddEntry(realHam);

        auto result = classifier.Classify(text, expectedResult);

        if (ClassifierAsserts::TestIfResultIsEqual(result, expectedResult))
        {
            std::cout << prefix <<  "OK." << std::endl;
        }
        else
        {
            std::cerr << prefix << " failed." << std::endl <<
                         "Test details:" << std::endl << "text = " << text << std::endl;
            throw FailedTest(prefix + " failed.\n");
        }
    }
private:
    unsigned long long spamCount;
    unsigned long long hamCount;
    ClassificationResult expectedResult;
};


#endif // CLASSIFIERTESTS_H
