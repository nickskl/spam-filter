#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <dirent.h>
#include <fstream>
#include <sys/stat.h>
#include "messagemeta.h"
#include "ui_mainwindow.h"
#include <nbclassifier.h>
#include <nsnbclassifier.h>
#include <osbfeatureextractor.h>
#include <characterngrammfeatureextractor.h>
#include <markovianfeatureextractor.h>
#include <qprogressdialog.h>
#include <regex>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onLoad();
    void onClassify();
    void onClassifyAll();
    void onTest();
    void onItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* old);
    void onSelectMailFolder();
    void onSelectTestFile();
    void onSaveSettings();
    void onDeleteMeta();
    void onCancelSettings();

    void LoadSettings();
    void SaveSettings();
    void isInUnclassified();
    void notInUnclassified();
    void reportMisclassification();
    void isTopLevelItem();

    void ShowContents(QTreeWidgetItem* item);
    void ClearContents();

signals:
    void unclassified();
    void classified();
    void topLevel();
    void needToShowContents(QTreeWidgetItem* item);
    void needToClearContents();

private:
    void ResetTreeView();
    void RefreshTreeView(const QString &filename, MessageMeta &filemeta, const char currentPosition);
    void DeleteAllTreeViewItems(QTreeWidgetItem* root);

    void LoadMeta();
    void SaveMeta();
    MessageMeta* MetaContains(const char *filename);
    void MetaAdd(const char *filename);

    Ui::MainWindow *ui;
    QString pathToConfig = "./config";
    QString pathToMail = "./mail";
    QString pathToMetafile = "./meta";
    QString pathToTestfile = "./test";
    int currentClassifier = 0;
    int currentFeatureExtractor = 0;
    int currentParameter = 5;

    std::vector<MessageMeta> meta;

    Classifier *classifier = nullptr;
};

#endif // MAINWINDOW_H
