#include "messagemeta.h"

MessageMeta::MessageMeta(const std::string &_filename, const ClassificationResult &_classificationResult)
    : filename(_filename), classificationResult(_classificationResult)
{
    std::hash<std::string> hashf;

    std::ifstream message;
    message.open(_filename);

    std::string buffer((std::istreambuf_iterator<char>(message)),
                     std::istreambuf_iterator<char>());
    hash = hashf(buffer);
}


std::ostream& operator<<(std::ostream& os, const MessageMeta& obj)
{
    os.write(reinterpret_cast<const char*>(&obj.hash), sizeof(obj.hash));
    size_t size = obj.filename.size();
    os.write(reinterpret_cast<const char*>(&size), sizeof(obj.filename.size()));
    os.write(obj.filename.c_str(), obj.filename.size());
    os.write(reinterpret_cast<const char*>(&obj.classificationResult), sizeof(obj.classificationResult));
    return os;
}

std::istream& operator>>(std::istream& is, MessageMeta& obj)
{
    size_t size;
    is.read(reinterpret_cast<char*>(&obj.hash), sizeof(obj.hash));
    is.read(reinterpret_cast<char*>(&size), sizeof(size));

    char* buffer = new char[size+1];
    is.read(buffer, size);
    buffer[size] = '\0';
    obj.filename = buffer;
    delete[] buffer;

    is.read(reinterpret_cast<char*>(&obj.classificationResult), sizeof(obj.classificationResult));
    return is;
}

bool MessageMeta::operator==(const MessageMeta& rhs)
{
    return (this->hash == rhs.hash)&&
            (this->filename == rhs.filename);
}

bool MessageMeta::operator!=(const MessageMeta& rhs)
{
    return !(this->operator==(rhs));
}
