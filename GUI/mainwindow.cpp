#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QList<QTreeWidgetItem*> list;
    QStringList strings;
    strings.append("Новые");
    list.append(new QTreeWidgetItem(strings));
    strings.clear();
    strings.append("Спам");
    list.append(new QTreeWidgetItem(strings));
    strings.clear();
    strings.append("Легитимная почта");
    list.append(new QTreeWidgetItem(strings));
    ui->mailTreeView->addTopLevelItems(list);

    LoadSettings();
    onLoad();

    connect(ui->classifyButton, SIGNAL(clicked(bool)), this, SLOT(onClassify()));
    connect(ui->testButton, SIGNAL(clicked(bool)), this, SLOT(onTest()));
    connect(ui->reportButton, SIGNAL(clicked(bool)), this, SLOT(reportMisclassification()));
    connect(ui->mailTreeView, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(onItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(this, SIGNAL(unclassified()), this, SLOT(isInUnclassified()));
    connect(this, SIGNAL(classified()), this, SLOT(notInUnclassified()));
    connect(this, SIGNAL(topLevel()), this, SLOT(isTopLevelItem()));
    connect(ui->classifyAllButton, SIGNAL(clicked(bool)), this, SLOT(onClassifyAll()));
    connect(this, SIGNAL(needToShowContents(QTreeWidgetItem*)), this, SLOT(ShowContents(QTreeWidgetItem*)));
    connect(this, SIGNAL(needToClearContents()), this, SLOT(ClearContents()));
    connect(ui->selectMailFolderButton, SIGNAL(clicked(bool)), this, SLOT(onSelectMailFolder()));
    connect(ui->selectTestfileButton, SIGNAL(clicked(bool)), this, SLOT(onSelectTestFile()));
    connect(ui->saveSettingsButton, SIGNAL(clicked(bool)), this, SLOT(onSaveSettings()));
    connect(ui->cancelButton, SIGNAL(clicked(bool)), this, SLOT(onCancelSettings()));
    connect(ui->deleteMetadata, SIGNAL(clicked(bool)), this, SLOT(onDeleteMeta()));

    FeatureExtractor* fe = new OSBFeatureExtractor(5);
    classifier = new NBClassifier("NB-" + fe->Signature(), fe);
}

MainWindow::~MainWindow()
{
    if(classifier)
        delete classifier;
    SaveMeta();
    SaveSettings();
    delete ui;
}

void MainWindow::LoadMeta()
{
    meta.clear();

    std::fstream metafile;
    metafile.open(pathToMetafile.toStdString(), std::ios_base::in | std::ios_base::out | std::ios_base::binary);

    size_t count = 0;
    metafile.read(reinterpret_cast<char*>(&count), sizeof(count));
    if(count > 0)
        meta.resize(count);

    for(size_t i = 0; i < count; i++)
    {
        metafile >> meta[i];
    }
    metafile.close();
}

void MainWindow::onItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* old)
{
    if ((old == nullptr)||(current == nullptr))
        return;
    if(current->parent() == nullptr)
    {
        emit topLevel();
        emit needToClearContents();
        return;
    }

    emit needToShowContents(current);

    if(current->parent() == old->parent())
        return;
    if(current->parent() == ui->mailTreeView->topLevelItem(0))
        emit unclassified();
    else
        emit classified();
}

void MainWindow::SaveMeta()
{
    std::ofstream metafile;
    metafile.open(pathToMetafile.toStdString(), std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);

    size_t count = meta.size();
    metafile.write(reinterpret_cast<char*>(&count), sizeof(count));

    for(size_t i = 0; i < count; i++)
    {
        metafile << meta[i];
    }
    metafile.close();
}


MessageMeta* MainWindow::MetaContains(const char* filename)
{
    MessageMeta newmeta(pathToMail.toStdString() + "/" + filename);
    for(auto filemeta = meta.begin(); filemeta != meta.end(); filemeta++)
    {
        if(newmeta == *filemeta)
            return &(*filemeta);
    }
    return nullptr;
}

void MainWindow::MetaAdd(const char* filename)
{
    MessageMeta newmeta(pathToMail.toStdString() + "/" + filename);
    meta.push_back(newmeta);
}

void MainWindow::ResetTreeView()
{
    for(int i = 0; i < ui->mailTreeView->topLevelItemCount(); i++)
    {
        for (int j = ui->mailTreeView->topLevelItem(i)->childCount()-1; j >= 0 ; j--)
            DeleteAllTreeViewItems(ui->mailTreeView->topLevelItem(i)->child(j));
    }
}

void MainWindow::DeleteAllTreeViewItems(QTreeWidgetItem *root)
{
    for(int i = 0; i < root->childCount(); i++)
    {
        DeleteAllTreeViewItems(root->child(i));
        root->removeChild(root->child(i));
    }

    delete root;
    root = nullptr;
}

void MainWindow::onLoad()
{
    ResetTreeView();
    DIR *dir;
    struct dirent *dirent;
    struct _stat stat;
    MessageMeta* currentMessageMeta = nullptr;

    LoadMeta();

    if ((dir = opendir(pathToMail.toStdString().c_str())) != NULL)
    {
        while ((dirent = readdir(dir)) != NULL)
        {
            if(strcmp(dirent->d_name, ".") != 0
                    && strcmp(dirent->d_name, "..") != 0)
            {
                QStringList entry;
                entry.append(QString(dirent->d_name));
                if(_stat(dirent->d_name, &stat) == -1)
                {
                    if((currentMessageMeta = MetaContains(dirent->d_name)))
                    {
                        ui->mailTreeView->topLevelItem((unsigned char)currentMessageMeta->GetClassificationResult())->addChild(new QTreeWidgetItem(entry));
                    }
                    else
                    {
                        MetaAdd(dirent->d_name);
                        ui->mailTreeView->topLevelItem(0)->addChild(new QTreeWidgetItem(entry));
                    }
                }
            }
        }
        closedir (dir);
    }
}

void MainWindow::RefreshTreeView(const QString& filename, MessageMeta& filemeta, const char currentPosition)
{
    if (currentPosition == static_cast<char>(filemeta.GetClassificationResult())) return;
    auto currentParent = ui->mailTreeView->topLevelItem(currentPosition);

    auto newParent = ui->mailTreeView->topLevelItem(static_cast<char>(filemeta.GetClassificationResult()));

    QTreeWidgetItem* item = nullptr;

    for(int i = 0; i < currentParent->childCount(); i++)
    {
        auto tmp = currentParent->child(i)->text(0);
        if(filename == tmp)
        {
            item = currentParent->child(i);
            currentParent->removeChild(item);
            break;
        }
    }

    if(item == nullptr)
        throw "err";

    newParent->addChild(item);
}

void MainWindow::onClassify()
{
    auto file = ui->mailTreeView->selectedItems().first()->text(0);
    auto filemeta = MetaContains(file.toLocal8Bit());
    if(!filemeta)
    {
        MetaAdd(file.toLocal8Bit());
        filemeta = MetaContains(file.toLocal8Bit());
    }

    std::ifstream message;
    message.open(pathToMail.toStdString() + "/" + file.toStdString());

    std::string buffer((std::istreambuf_iterator<char>(message)),
                     std::istreambuf_iterator<char>());

    char oldResult = static_cast<char>(filemeta->GetClassificationResult());
    filemeta->GetClassificationResult() = classifier->Classify(buffer);

    RefreshTreeView(file, *filemeta, oldResult);
}

void MainWindow::onClassifyAll()
{
    int numTasks = ui->mailTreeView->topLevelItem(0)->childCount();

    QProgressDialog progress("Обработка запроса...", "Отмена", 0, numTasks, this);
    progress.setWindowModality(Qt::WindowModal);

    for(int i = 0; i < numTasks; i++)
    {
        progress.setValue(i);
        if (progress.wasCanceled())
            break;
        ui->mailTreeView->topLevelItem(0)->child(0)->setSelected(true);
        onClassify();
    }
    progress.setValue(numTasks);
}

void MainWindow::onTest()
{
    auto testfile = pathToTestfile.toStdString();
    ui->testResultLog->clear();

    std::ifstream file;
    file.open(testfile);
    std::vector<std::string> tasks;

    char buffer[256];
    while(file)
    {
        file.getline(buffer, 256);
        if(buffer[0] == '\0')
            continue;
        buffer[file.gcount()] = 0;
        tasks.push_back(buffer);
    }
    file.close();

    int totalCount = 0;
    int totalHam = 0;
    int totalSpam = 0;
    int correctHam = 0;
    int correctSpam = 0;
    int falseHam = 0;
    int falseSpam = 0;

    std::regex regex("(\\w+) (.*)");
    classifier->SetSaveDictionary(false);

    int numTasks = tasks.size();

    QProgressDialog progress("Обработка запроса...", "Отмена", 0, numTasks, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setWindowTitle("Тестирование...");
    int i = 0;

    for(auto string = tasks.begin(); string != tasks.end(); string++)
    {
        progress.setValue(i++);
        if (progress.wasCanceled())
            break;

        std::regex_iterator<std::string::iterator> regexIterator( string->begin(), string->end(), regex);
        std::regex_iterator<std::string::iterator> rxend;

        while (regexIterator != rxend)
        {
            ClassificationResult expectedResult;
            auto correctClass = (*regexIterator)[1].str();
            auto path = (*regexIterator)[2].str();
            std::ifstream file;
            file.open(path);
            if(!file)
            {
                throw std::exception();
            }

            std::string text;
            file.seekg(0, std::ios::end);
            text.reserve(file.tellg());
            file.seekg(0, std::ios::beg);

            text.assign((std::istreambuf_iterator<char>(file)),
                        std::istreambuf_iterator<char>());
            file.close();

            if(correctClass == "spam")
                expectedResult = ClassificationResult::spam;
            else if (correctClass == "ham")
                expectedResult = ClassificationResult::nonSpam;
            else
            {
                throw std::exception();
            }
            auto result = classifier->Classify(text);

            totalCount++;
            if(expectedResult == ClassificationResult::spam)
            {
                totalSpam++;
                if(result == expectedResult)
                    correctSpam++;
                else
                    falseSpam++;
            }
            else
            {
                totalHam++;
                if(result == expectedResult)
                    correctHam++;
                else
                    falseHam++;
            }
           regexIterator++;
        }
    }
    progress.setValue(i);

    char result[256];
    sprintf(result, "Accuracy: %.4f\nFPR: %.4f\nTPR %.4f\nFNR %.4f\nTNR %.4f\n",
            (correctSpam+correctHam)/(double)totalCount, falseSpam/(double)totalSpam,
            correctSpam/(double)totalSpam, falseHam/(double)totalHam, correctHam/(double)totalHam);
    ui->testResultLog->setPlainText(result);
    classifier->SetSaveDictionary(true);
}

void MainWindow::isInUnclassified()
{
    ui->classifyButton->setEnabled(true);
    ui->classifyAllButton->setEnabled(true);
    ui->reportButton->setEnabled(false);
}

void MainWindow::notInUnclassified()
{
    ui->classifyButton->setEnabled(false);
    ui->classifyAllButton->setEnabled(false);
    ui->reportButton->setEnabled(true);
}

void MainWindow::isTopLevelItem()
{
    ui->classifyButton->setEnabled(false);
    ui->classifyAllButton->setEnabled(false);
    ui->reportButton->setEnabled(false);
}

void MainWindow::reportMisclassification()
{
    auto file = ui->mailTreeView->selectedItems().first()->text(0);
    auto filemeta = MetaContains(file.toLocal8Bit());
    if(!filemeta)
    {
        MetaAdd(file.toLocal8Bit());
        filemeta = MetaContains(file.toLocal8Bit());
    }

    auto filename = pathToMail;
    filename.append("/");
    filename.append(file);

    std::ifstream message;
    message.open(filename.toLocal8Bit());

    std::string buffer((std::istreambuf_iterator<char>(message)),
                     std::istreambuf_iterator<char>());

    char oldResult = static_cast<char>(filemeta->GetClassificationResult());
    ClassificationResult expectedResult;
    if (filemeta->GetClassificationResult() == ClassificationResult::spam)
        expectedResult = ClassificationResult::nonSpam;
    else
        expectedResult = ClassificationResult::spam;

    filemeta->GetClassificationResult() = classifier->Classify(buffer, expectedResult);

    RefreshTreeView(file, *filemeta, oldResult);
}

void MainWindow::ShowContents(QTreeWidgetItem* item)
{
    auto file = item->text(0).toStdString();

    std::ifstream message;
    message.open(pathToMail.toStdString() + "/" + file);

    std::string buffer((std::istreambuf_iterator<char>(message)),
                     std::istreambuf_iterator<char>());

    ui->messageView->setPlainText(buffer.c_str());
}

void MainWindow::ClearContents()
{
    ui->messageView->clear();
}

void MainWindow::onSelectMailFolder()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Выберите папку"), "./",
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(dir != "")
        ui->pathToMailEdit->setText(dir);
}

void MainWindow::onSelectTestFile()
{
    QString dir = QFileDialog::getOpenFileName(this, tr("Выберите файл"), "./");
    if(dir != "")
        ui->pathToTestfileEdt->setText(dir);
}

void MainWindow::onSaveSettings()
{
    SaveMeta();
    if ((ui->classifierBox->currentIndex() != currentClassifier)||
        (ui->feBox->currentIndex() != currentFeatureExtractor)||
        (ui->kBox->value() != currentParameter))
    {
        currentClassifier = ui->classifierBox->currentIndex();
        currentFeatureExtractor = ui->feBox->currentIndex();
        currentParameter = ui->kBox->value();

        if(classifier != nullptr)
            delete classifier;

        FeatureExtractor* fe;

        switch(currentFeatureExtractor)
        {
        case 0:
            fe = new OSBFeatureExtractor(currentParameter);
            break;
        case 1:
            fe = new CharacterNGrammFeatureExtractor(currentParameter);
            break;
        case 2:
            fe = new MarkovianFeatureExtractor(currentParameter);
            break;
        default:
            fe = nullptr;
        }

        if(fe != nullptr)
            switch(currentClassifier)
            {
            case 0:
                classifier = new NBClassifier("NB-" + fe->Signature(), fe);
                break;
            case 1:
                classifier = new NSNBClassifier("NSNB-" + fe->Signature(), fe);
                break;
            case 2:
                classifier = new modNSNBClassifier("modNSNB-" + fe->Signature(), fe);
                break;
            default:
                classifier = nullptr;
            }
        else
            classifier = nullptr;
    }

    pathToTestfile = ui->pathToTestfileEdt->text();

    if (pathToMail != ui->pathToMailEdit->text())
    {
        pathToMail = ui->pathToMailEdit->text();
        this->onLoad();
    }
}

void MainWindow::onDeleteMeta()
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Подтверждение", "Удалить метаданные?",
                                    QMessageBox::Yes|QMessageBox::No);
      if (reply == QMessageBox::Yes)
      {
          meta.clear();
          SaveMeta();
          this->onLoad();
      }
}

void MainWindow::onCancelSettings()
{
    ui->classifierBox->setCurrentIndex(currentClassifier);
    ui->feBox->setCurrentIndex(currentFeatureExtractor);
    ui->kBox->setValue(currentParameter);
    ui->pathToTestfileEdt->setText(pathToTestfile);
    ui->pathToMailEdit->setText(pathToMail);
}

void MainWindow::LoadSettings()
{
    std::ifstream file;
    file.open(pathToConfig.toStdString(), std::ios_base::in | std::ios_base::binary);

    if(!file)
        SaveSettings();
    else
    {
        char *buffer;
        int size = 0;
        file.read(reinterpret_cast<char*>(&currentClassifier), sizeof(currentClassifier));
        file.read(reinterpret_cast<char*>(&currentFeatureExtractor), sizeof(currentFeatureExtractor));
        file.read(reinterpret_cast<char*>(&currentParameter), sizeof(currentParameter));
        file.read(reinterpret_cast<char*>(&size), sizeof(size));
        buffer = new char[size+1];
        file.read(buffer, size);
        buffer[size] = '\0';
        pathToMail = buffer;
        delete buffer;
        file.read(reinterpret_cast<char*>(&size), sizeof(size));
        buffer = new char[size+1];
        file.read(buffer, size);
        buffer[size] = '\0';
        pathToTestfile = buffer;
        delete buffer;
        onCancelSettings();
    }
}

void MainWindow::SaveSettings()
{
    std::ofstream file;
    file.open(pathToConfig.toStdString(), std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);

    file.write(reinterpret_cast<char*>(&currentClassifier), sizeof(currentClassifier));
    file.write(reinterpret_cast<char*>(&currentFeatureExtractor), sizeof(currentFeatureExtractor));
    file.write(reinterpret_cast<char*>(&currentParameter), sizeof(currentParameter));
    int size = pathToMail.length();
    file.write(reinterpret_cast<char*>(&size), sizeof(size));
    file.write(pathToMail.toStdString().c_str(), size);
    size = pathToTestfile.length();
    file.write(reinterpret_cast<char*>(&size), sizeof(size));
    file.write(pathToTestfile.toStdString().c_str(), size);
    file.close();
}
