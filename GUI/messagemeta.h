#ifndef MESSAGEMETA_H
#define MESSAGEMETA_H

#include <string>
#include <classifier.h>
#include <istream>

class MessageMeta
{
public:
    MessageMeta() { }
    MessageMeta(const std::string& _filename, const ClassificationResult& _classificationResult = ClassificationResult::none);

    friend std::ostream& operator<<(std::ostream& os, const MessageMeta& obj);
    friend std::istream& operator>>(std::istream& is, MessageMeta& obj);

    bool operator==(const MessageMeta& rhs);
    bool operator!=(const MessageMeta& rhs);

    ClassificationResult& GetClassificationResult() { return classificationResult; }

private:
    size_t hash;
    std::string filename;
    ClassificationResult classificationResult;
};

#endif // MESSAGEMETA_H
